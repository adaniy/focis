<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFiles extends Model
{
    protected $guarded = [];

    
    public function Files()
    {
        return $this->belongsTo('App\Files', 'file_id', 'id');
    }
    
}
