<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Files;
use App\Group;
use App\UserFiles;
use App\GroupFile;
use App\Category;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Response;
use Auth;
use finfo;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $FileID = $id;
        $File = Files::find($FileID);        
        $category = Category::where('id', $File->category_id )->first();
        return view('Files.index',compact('FileID', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    function get_mime_type($file) {
        // dd($file ,   mime_content_type($file));
        // dd(extension_loaded('fileinfo'));
        
        // dd($file->getClientOriginalExtension());
        $mtype = false;
        if (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mtype = finfo_file($finfo, $file);
            finfo_close($finfo);
        } elseif (function_exists('mime_content_type')) {
            $mtype = mime_content_type($file);
        } 
        return $mtype;
    }

    function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
    
        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 
    
        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow)); 
    
        return round($bytes, $precision) . ' ' . $units[$pow]; 
    } 

    public function store(Request $request , $id)
    {
        // dd($request->all());
        $file = $request->file('file');
        $path = '/files';
        $serverPath = null;
        $serverPathArr = [];
        if ($file) {
            // for ($i = 0; $i < count($file); $i++) {
                $destinationPath = public_path($path); // path to save to, has to exist and be writeable
                $fileSize = $file->getClientSize();
                if (($fileSize == false)) {
                    return response()->json(['upload_status' =>  'error Your file `' . $file->getClientOriginalName() . '` exceeds your upload max filesize (limit is 200M)'], 401);
                }
                $FileType = $this->get_mime_type($file);
                // dd($x);
                if( $file->getClientOriginalExtension() == 'jpeg' || $file->getClientOriginalExtension() == 'png'||
                    $file->getClientOriginalExtension() == 'jpg' || $file->getClientOriginalExtension() == 'gif'||
                    $file->getClientOriginalExtension() == 'svg' )
                {   
                    $fileType = "image";
                }
                 else if ($file->getClientOriginalExtension() == 'pdf' ){
                    $fileType = "pdf";
                }
                else {
                    $fileType = "-";
                }

                $last_id = Files::latest()->first();
                if ($last_id!= null){
                    $last_id = $last_id->id + 1;
                }
                else{
                    $last_id = 1;
                } 
                
                $uniqueFileName = $last_id .'_'.time() . '.' .  $file->getClientOriginalExtension() ; 
                $filename = $file->getClientOriginalName(); // original name that it was uploaded with
                $file->move($destinationPath, $uniqueFileName); // moving the file 
                
                $serverPath = $path . '/' . $uniqueFileName;
                $serverPathArr[] =  $serverPath;
                $Files = Files::create([
                    'category_id' =>  $id,
                    'title' => $filename,
                    'name' => $uniqueFileName,
                    'src' => $serverPath,
                    'file_type' => $fileType,
                    'file_size' => $this->formatBytes($fileSize),
                ]);
                sleep(1);

                $Groups = Group::all();
                if(count($Groups) > 0 ){
                    for($i = 0; $i < count($Groups); $i++){
                        $GroupFile = GroupFile::create([
                            'file_id' => $Files->id,
                            'group_id' => $Groups[$i]->id,
                        ]);
                    }
                }
              
              
            // }
        }
        if ($Files)
            return response()->json(['upload_status' =>  'success'], 200);
        else
            return redirect()->back()->with('message', 'Sorry, something went wrong ');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Files  $files
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, Request $request, $id)
    {
        // dd( $category->all(), $request->all() , $request->parent );
        $CategoryType = 'list';
        $File = Files::find($id);        
        $category = $category::where('id', $File->category_id )->first();

        $Files = DB::select(
            "select DISTINCT files.*, user_files.is_read from `files` 
            inner join `categories` on `files`.`category_id` = `categories`.`id`
            left join `user_files` on `user_files`.`file_id` = `files`.`id` and `user_files`.`user_id` = ". Auth::user()->id ."
            left join `group_file` on `group_file`.`file_id` = `files`.`id`
            WHERE `group_file`.`group_id` in (
                SELECT `user_group`.`group_id` FROM users 
                left join `user_group` on `user_group`.`user_id` = `users`.`id` 
                WHERE `users`.`id` = ". Auth::user()->id ."
            )
            AND files.id = ".  $id ."
            order by `user_files`.`is_read` asc"
        );

        $view = view("Files.file",compact('category', 'CategoryType' , 'Files'))->render();

        return response()->json(['html'=>$view]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Files  $files
     * @return \Illuminate\Http\Response
     */
    public function edit(Files $files)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Files  $files
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Files $files)
    {
        $Files = Files::where('id' , $request['edit_fileID'] )->update([
            'title' => $request['edit_fileName'],
        ]);
        if ($Files)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Files  $files
     * @return \Illuminate\Http\Response
     */
 

    public function destroy(Request $request)
    {
        $Files = Files::find( $request['deleteID']);
        $folder_path = "files"; 
        
        // specified folder 
        $files = glob($folder_path.'/' . $Files->name);  
        // Deleting all the files in the list 
        foreach($files as $file) { 
            if (unlink($file)){
                $Files = Files::destroy( $request['deleteID']);
            };  
        } 
      
        if (!$Files)
        {
            return Response::json(['status' => 'fail']);
        }
        else
        {
            return Response::json(['status' => 'success']);
        }
    }

    public function LatestFiles(Request $request)
    {
        $Files = DB::select(
            "select DISTINCT files.*, user_files.is_read from `files` 
            left join `user_files` on `user_files`.`file_id` = `files`.`id` and `user_files`.`user_id` = ". Auth::user()->id ."
            left join `group_file` on `group_file`.`file_id` = `files`.`id`
            WHERE `group_file`.`group_id` in (
                SELECT `user_group`.`group_id` FROM users 
                left join `user_group` on `user_group`.`user_id` = `users`.`id` 
                WHERE `users`.`id` = ". Auth::user()->id ."
            )
            AND (`user_files`.`is_read` = 0 OR `user_files`.`is_read` is null)
            AND `files`.`created_at` between date_sub(now(),INTERVAL 1 WEEK) and now()
            order by `files`.`created_at` desc"
        );

        
        // $Files = Files::orderBy('id', 'desc')->take(5)->get();
        $category = [];
        $CategoryType = $request->type;
        if ( $CategoryType == 'grid'){
            $view = view("include.category-grid",compact('category', 'CategoryType' ,'Files'))->render();
        }
        else 
        {
            $view = view("include.Latest-files",compact('Files'))->render();
        }

        return response()->json(['html'=>$view]);

        
    }

    public function LatestFiles1(Request $request)
    {
        $start_week = date("Y-m-d",strtotime("-7 days"));
        $end_week = date("Y-m-d",strtotime("+1 days"));
        // dd($start_week, $end_week);  
        $Files = Files::leftjoin('user_files', function($join)
            {
                $join->on('user_files.file_id','=','files.id')->where('user_files.user_id','=',Auth::user()->id);
            }
        );
        // ->leftjoin('users', 'user_files.user_id', 'users.id');
        $Files = $Files->whereRaw(' user_files.is_read = 0 OR user_files.is_read is null' );
        // $Files = $Files->whereRaw('( users.id='.Auth::user()->id.') OR users.id is null' );
        $Files = $Files->whereBetween('files.created_at', [$start_week, $end_week]);
        $Files = $Files->select(DB::raw('files.* , user_files.is_read'));
        $Files = $Files->orderBy('files.id','desc')
        ->get();
        // ->toSql();
        // dd($Files);
        // return response::json(['data' => $Files ]);
        

        // $Files = Files::orderBy('id', 'desc')->take(5)->get();
        $category = [];
        $CategoryType = $request->type;
        if ( $CategoryType == 'grid'){
            $view = view("include.category-grid",compact('category', 'CategoryType' ,'Files'))->render();
        }
        else 
        {
            $view = view("include.Latest-files",compact('Files'))->render();
        }

        return response()->json(['html'=>$view]);

        
    }

    public function AllFilesForReading(Request $request)
    {
        $Files = Files::leftjoin('user_files', function($join)
            {
                $join->on('user_files.file_id','=','files.id')->where('user_files.user_id','=',Auth::user()->id);
            }
        );
        // ->leftjoin('users', 'user_files.user_id', 'users.id');
        // $Files = $Files->whereRaw(' user_files.is_read = 0 OR user_files.is_read is null' );
        // $Files = $Files->whereRaw('( users.id='.Auth::user()->id.') OR users.id is null' );
        $Files = $Files->select(DB::raw('files.* , user_files.is_read'));
        $Files = $Files->orderBy('user_files.is_read','asc')
        // ->toSql();
        ->get();
        // dd($Files);
        // return response::json(['data' => $Files ]);
        // $Files = Files::orderBy('id', 'desc')->take(5)->get();
        $category = [];
        $CategoryType = $request->type;
        if ( $CategoryType == 'grid'){
            $view = view("include.category-grid",compact('category', 'CategoryType' ,'Files'))->render();
        }
        else 
        {
            $view = view("include.all-files",compact('Files'))->render();
        }

        return response()->json(['html'=>$view]);

        
    }

    public function Reminders(Request $request)
    {
        $Files = DB::select(
            "select DISTINCT files.*, user_files.is_read from `files` 
            left join `user_files` on `user_files`.`file_id` = `files`.`id` and `user_files`.`user_id` = ". Auth::user()->id ."
            left join `group_file` on `group_file`.`file_id` = `files`.`id`
            WHERE `group_file`.`group_id` in (
                SELECT `user_group`.`group_id` FROM users 
                left join `user_group` on `user_group`.`user_id` = `users`.`id` 
                WHERE `users`.`id` = ". Auth::user()->id ."
            )
            AND (`user_files`.`is_read` = 0 OR `user_files`.`is_read` is null)
            AND `files`.`created_at` < CURDATE() 
            AND `files`.`need_confirm` = 1 
            order by `files`.`created_at` desc"
        );


        // dd($Files);
            $view = view("include.reminders",compact('Files'))->render();

        return response()->json(['html'=>$view]);
        
    }
   

    public function MarkFileAsRead(Request $request)
    {
        // dd($request->all(), intval($request->isRead)) ;
        $UserFiles = UserFiles::where('user_id' , Auth::user()->id )->where('file_id' , $request->id )->first();
        if ($UserFiles == null)
        {
            $UserFiles = UserFiles::create([
                'user_id' => Auth::user()->id,
                'file_id' => $request->id,
                'is_read' => intval($request->isRead),
            ]);
        }
        else 
        {
            $UserFiles->is_read = intval($request->isRead);
            $UserFiles->update();
        }
       
        if ($UserFiles)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
    }

    public function ConfirmReading(Request $request)
    {
        // dd($request->all());
        $Files = Files::where('id', $request['id'])->update(['need_confirm' => $request['Confirm']]);
        if ($Files)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
            
    }
}
