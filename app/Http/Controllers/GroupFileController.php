<?php

namespace App\Http\Controllers;

use App\Files;
use App\Group;
use App\GroupFile;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function GroupsAndFiles()
    {
        $Files = Files::with('Group')->orderBy('created_at','DESC')->get();
        $Groups = Group::with('File')->orderBy('name')->get();
        return Response::json([
            'Files' => $Files,
            'Groups' => $Groups,
        ]);
    }

    public function FileGroups($id)
    {
        $Files = Files::where('id', $id)->with('Group')->orderBy('created_at','DESC')->get();
        $Groups = Group::with('File')->orderBy('name')->get();
        return Response::json([
            'Files' => $Files,
            'Groups' => $Groups,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store2(Request $request)
    {
        // dd($request->all());
        // dd($request['checkbox']);
        if ($request['checkbox'] != null) {
            DB::beginTransaction();
            try {
                $DeleteGroupFiles = GroupFile::truncate();
                foreach ($request['checkbox'] as $FileID => $GroupIDs) {
                    // dd($FileID, $GroupIDs);
                    for ($i = 0; $i < count($GroupIDs); $i++) {
                        $CreateGroupFile[] = GroupFile::create([
                            'File_id' => $FileID,
                            'group_id' => $GroupIDs[$i],
                        ]);
                    }
                }
                $success = true;
                DB::commit();
            } 
            catch (\Exception $e) {
                $success = false;
                DB::rollback();
                return Response::json(['status' => 'fail']);
                // dd($e);
            }
        }

        if ($success) {
            return Response::json(['status' => 'success']);
        }
        return Response::json(['status' => 'fail']);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // dd($request['checkbox']);

        // check if all file data at files DataTable was sent with out filters
        $GroupFiles = GroupFile::whereNotIn('file_id', $request['files'])->get()->toArray();
        if (count($GroupFiles) == 0 ){
            // ($GroupFiles == 0 ) => no filters at files DataTable
            if ($request['checkbox'] != null) {
                DB::beginTransaction();
                try {
                    $DeleteGroupFiles = GroupFile::truncate(); // delete all data at GroupFile table to create new GroupFile
                    foreach ($request['checkbox'] as $fileID => $GroupIDs) {
                        // dd($fileID, $GroupIDs);
                        for ($i = 0; $i < count($GroupIDs); $i++) {
                            $CreateGroupFile[] = GroupFile::create([
                                'file_id' => $fileID,
                                'group_id' => $GroupIDs[$i],
                            ]);
                        }
                    }
                    $success = true;
                    DB::commit();
                } 
                catch (\Exception $e) {
                    $success = false;
                    DB::rollback();
                    return Response::json(['status' => 'fail']);
                    // dd($e);
                }
            }
            else{
                // ($request['checkbox'] == null) => delete all GroupFile
                $DeleteGroupFiles = GroupFile::truncate();
                $success = true;
            }
        }
        else {
            if ($request['checkbox'] != null) {
                DB::beginTransaction();
                try {
                    for ($i = 0; $i < count($request['files']); $i++) {
                        // delete just $request['files'] (files have been filtered) at GroupFile table to create new GroupFile
                        $DeleteGroupFiles = GroupFile::where('file_id', $request['files'][$i])->delete(); 
                    }
                    foreach ($request['checkbox'] as $fileID => $GroupIDs) {
                        // dd($fileID, $GroupIDs);
                        for ($i = 0; $i < count($GroupIDs); $i++) {
                            $CreateGroupFile[] = GroupFile::create([
                                'file_id' => $fileID,
                                'group_id' => $GroupIDs[$i],
                            ]);
                        }
                    }
                    $success = true;
                    DB::commit();
                } 
                catch (\Exception $e) {
                    $success = false;
                    DB::rollback();
                    return Response::json(['status' => 'fail']);
                    // dd($e);
                }
            }
            else{
                // ($request['checkbox'] == null) => delete $request['files'] GroupFile
                for ($i = 0; $i < count($request['files']); $i++) {
                    // delete just $request['files'] (files have been filtered) at GroupFile table
                    $DeleteGroupFiles = GroupFile::where('file_id', $request['files'][$i])->delete(); 
                }
                $success = true;
            }
            // dd($request->all() , $GroupFiles);
        }

        if ($success) {
            return Response::json(['status' => 'success']);
        }
        return Response::json(['status' => 'fail']);
    }
    
    
    public function store1(Request $request)
    {
        // dd($request->all());
        // dd($request['checkbox']);
        if ($request['checkbox'] != null) {
            foreach ($request['checkbox'] as $FileID => $GroupIDs) {
                dd($FileID, $GroupIDs);
                $GroupFiles = GroupFile::where('File_id', $FileID)->get();
                if ($GroupFiles) {
                    DB::beginTransaction();
                    try {
                        // dd($GroupFiles);
                        // dd($GroupIDs, $FileID);
                        // $DeleteGroupFiles = GroupFile::where('File_id', $FileID)->whereIn('group_id',$GroupIDs)->delete();
                        // for ($i = 0; $i < count($GroupIDs); $i++){
                        // $DeleteGroupFiles = GroupFile::where('File_id', $FileID)->delete();
                        $DeleteGroupFiles = GroupFile::truncate();
                        // }
                        // dd($DeleteGroupFiles);
                        
                        // $ShowGroupFiles = GroupFile::where('File_id', $FileID)->get();
                        // dd($ShowGroupFiles);
                        // dd($GroupFiles);
                        for ($i = 0; $i < count($GroupIDs); $i++) {
                            $CreateGroupFile[] = GroupFile::create([
                                'File_id' => $FileID,
                                'group_id' => $GroupIDs[$i],
                            ]);
                        }

                        // dd($CreateGroupFile);
                        $success = true;
                        DB::commit();
                    } catch (\Exception $e) {
                        $success = false;
                        DB::rollback();
                        dd($e);
                    }
                }
            }
        }

        if ($success) {
            return Response::json(['status' => 'success']);
        }
        return Response::json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GroupFile  $GroupFile
     * @return \Illuminate\Http\Response
     */
    public function show(GroupFile $GroupFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GroupFile  $GroupFile
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupFile $GroupFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GroupFile  $GroupFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupFile $GroupFile)
    {
        // dd($request->all());
        if ($request['checkbox'] != null) {
            DB::beginTransaction();
            try {
                $DeleteGroupFiles = GroupFile::where('file_id', $request['id'])->delete();
                // $DeleteGroupFiles = GroupFile::truncate();

                foreach ($request['checkbox'] as $FileID => $GroupIDs) {
                    // dd($FileID, $GroupIDs);
                    for ($i = 0; $i < count($GroupIDs); $i++) {
                        $CreateGroupFile[] = GroupFile::create([
                            'File_id' => $FileID,
                            'group_id' => $GroupIDs[$i],
                        ]);
                    }
                }
                $success = true;
                DB::commit();
            } 
            catch (\Exception $e) {
                $success = false;
                DB::rollback();
                return Response::json(['status' => 'fail']);
                // dd($e);
            }
        }
        else {
            $DeleteGroupFiles = GroupFile::where('file_id', $request['id'])->delete();
            $success = true;
        }

        if ($success) {
            return Response::json(['status' => 'success']);
        }
        return Response::json(['status' => 'fail']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupFile  $GroupFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupFile $GroupFile)
    {
        //
    }
}
