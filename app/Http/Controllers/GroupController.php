<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use App\UserGroup;
use App\GroupFile;
use Illuminate\Http\Request;
use DataTables;
use Response;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetGroup()
    {
        $Group = Group::get();
        // dd($Group);
        return Datatables::of($Group)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $Group = Group::create([
            'name' => $request['new_g_name'],
        ]);

        if ($Group)
        {
            return Response::json(['status' => 'success']);
        }
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group, $id)
    {
        $Group = Group::with('User')->with('File')->find($id);
        if ($Group != null ){
            return response()->json(['status' => 'success' , 'Group' => $Group]);
        }
        else 
            return response()->json(['status' => 'fail' ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $Group = Group::find($request['edit_g_id']);
        // dd($User);
        $Group = $Group->update([
            'name' => $request['edit_g_name'],
        ]);

        if ($Group)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $UserGroup = UserGroup::where('group_id' , $request['deleteID'])->get();
        if ($UserGroup == null || count($UserGroup) == 0 ){
            // dd($UserGroup);
            $Group = Group::destroy($request['deleteID']);
            $UserGroup = UserGroup::where('group_id',$request['deleteID'])->delete();
            $GroupFile = GroupFile::where('group_id',$request['deleteID'])->delete();
            return Response::json(['status' => 'success']);
        }
        else{
            return Response::json(['status' => 'fail', 'message' => 'Sorry, this group contains users!']);
        }
    }


 
}
