<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Category;
use App\Files;
use Illuminate\Http\Request;
use Response;
use Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Category = Category::create([
            'name' => $request['new_name'],
            'parent_id' => $request['new_parent'] != null ? $request['new_parent'] : 0 ,
        ]);
        if ($Category)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show_id($id)
    {
        $cat=Category::orderBy('parent_id')->get();
        $json=[];
        foreach($cat as $c)
        {
            if($c->parent_id==0)
            {
                $t=['name'=>$c->name,"id"=>$c->id,"parent_id"=>$c->parent_id,"arr_parent_id"=>[]];
                $json[$c->id]=$t;
            }
            else
            {
                $t_old=$json[$c->parent_id]["arr_parent_id"];
                array_push( $t_old,$c->parent_id);
                $t=[
                    'name'=>$c->name,
                    "id"=>$c->id,
                    "parent_id"=>$c->parent_id,
                    "arr_parent_id"=>$t_old,
                ];
                $json[$c->id]=$t;
            }
        }
        // dd($json);
        $parint="";
        
        foreach($json[$id]["arr_parent_id"] as $P)
        {
            $parint .= '<li class="breadcrumb-item small " onclick="reOpenPath(this, '. $json[$P]["parent_id"]  .' , `'. $json[$P]["name"]   .'` )">'. $json[$P]["name"]  .'</li>';

        }
        // '<li class="breadcrumb-item small active" onclick="reOpenPath(this, '+ parent +' , `'+ name +'` )">'+ name +'</li>';
        // $parint .= '<li class="breadcrumb-item small active" onclick="reOpenPath(this, '. $json[$id]["parent_id"] .' , `'. $json[$id]["name"] .'` )">'. $json[$id]["name"] .'</li>';
        $cat=[$id,$json[$id]["name"]];
        return view('Files.manuals',compact('parint','cat'));
    }
    

    public function Sidebar()
    {
        $cat = Category::orderBy('parent_id')->where('view_at_sidebar',1)->get();
        $json = [];
        
        // dd(array_key_exists( 0,$json));
        foreach($cat as $c)
        {
            if(!array_key_exists( $c->parent_id,$json) )
            {
                $t = ['name' => $c->name, "id" => $c->id, "child"=>[]];
                $json[$c->id] = $t;
            }
            else
            {
                $t =[
                    'name' => $c->name,
                    "id" => $c->id,
                    "child" => [],
                ];
                $t_old=$json[$c->parent_id]["child"];
                array_push( $t_old,$t);
                $json[$c->parent_id]["child"]=$t_old;
                // dd($json[$c->parent_id]["child"],array_key_exists( $c->parent_id,$json));
                
            }
        }
        // dd($json);

        
    }
    public function show(Category $category, Request $request)
    {
        // dd( $category->all(), $request->all() , $request->parent );

        $CategoryType = $request->type;
        $category = $category::where('parent_id', $request->parent  )->get();

        $Files = DB::select(
            "select DISTINCT files.*, user_files.is_read from `files` 
            inner join `categories` on `files`.`category_id` = `categories`.`id`
            left join `user_files` on `user_files`.`file_id` = `files`.`id` and `user_files`.`user_id` = ". Auth::user()->id ."
            left join `group_file` on `group_file`.`file_id` = `files`.`id`
            WHERE `group_file`.`group_id` in (
                SELECT `user_group`.`group_id` FROM users 
                left join `user_group` on `user_group`.`user_id` = `users`.`id` 
                WHERE `users`.`id` = ". Auth::user()->id ."
            )
            AND categories.id = ".  $request->parent ."
            order by `user_files`.`is_read` asc"
        );


        // dd($Files);
        if ( $CategoryType == 'grid'){
            $view = view("include.category-grid",compact('category', 'CategoryType' , 'Files'))->render();
        }
        else 
        {
            $view = view("include.category-list",compact('category' , 'CategoryType' , 'Files'))->render();
        }

        return response()->json(['html'=>$view]);
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $Category = Category::where('id' , $request['edit_catID'] )->update([
            'name' => $request['edit_name'],
        ]);
        if ($Category)
            return Response::json(['status' => 'success']);
        else 
            return Response::json(['status' => 'fail']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request )
    {
        $hasChild = false;
        $Category = Category::all();
        $Files = Files::all();
        foreach($Category as $d)
        {
            if($d->parent_id ==  $request['deleteID'])
            {
                $hasChild = true;
            }
        }

        foreach($Files as $d)
        {
            if($d->category_id ==  $request['deleteID'])
            {
                $hasChild = true;
            }
        }
        if($hasChild)
        {
            return Response::json(['status' => 'fail']);
        }
        else
        {
            $Category = Category::destroy( $request['deleteID']);
            return Response::json(['status' => 'success']);

        }

        
    }
}
