<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Rank;
use Illuminate\Http\Request;
use DataTables;
use Response;
use Auth;
use Hash;
// use Excel;   `
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Image;
use Excel;


class MailController extends Controller
{
    public static function Mail( $title, $subject, $to, $cc, $bcc, $body)
    {

        $message ='';
        $message ='
        <html xmlns:v="urn:schemas-microsoft-com:vml" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <style type="text/css">
                    #outlook a {
                        padding: 0;
                    }
                    body {
                        width: 100% !important;
                        -webkit-text-size-adjust: 100%;
                        -ms-text-size-adjust: 100%;
                        margin: 0;
                        padding: 0;
                    }
                    .ExternalClass {
                        width: 100%;
                    }
                    .ExternalClass,
                    .ExternalClass p,
                    .ExternalClass span,
                    .ExternalClass font,
                    .ExternalClass td,
                    .ExternalClass div {
                        line-height: 100%;
                    }
                    #backgroundTable {
                        margin: 0;
                        padding: 0;
                        width: 100% !important;
                        line-height: 100% !important;
                        table-layout: fixed;
                        min-width: 280px;
                    }
                    img {
                        outline: none;
                        text-decoration: none;
                        border: none;
                        -ms-interpolation-mode: bicubic;
                    }
                    a img {
                        border: none;
                    }
                    .image_fix {
                        display: block;
                    }
                    p {
                        margin: 0px 0px !important;
                    }
            
                    a {
                        color: #0a8cce;
                        text-decoration: none;
                    }
            
                    table[class=full] {
                        width: 100%;
                        clear: both;
                        min-width: 280px;
                    }
            
                    table table {
                        table-layout: auto;
                    }
            
                    @media only screen and (max-width: 640px) {
            
                        a[href^="tel"],
                        a[href^="sms"] {
                            text-decoration: none;
                            color: #0a8cce;
                            pointer-events: none;
                            cursor: default;
                        }
            
                        .mobile_link a[href^="tel"],
                        .mobile_link a[href^="sms"] {
                            text-decoration: default;
                            color: #0a8cce !important;
                            pointer-events: auto;
                            cursor: default;
                        }
            
                        table[class=devicewidth] {
                            width: 440px !important;
                            text-align: center !important;
                        }
            
                        table[class=devicewidthinner] {
                            width: 420px !important;
                            text-align: center !important;
                        }
            
                        img[class=banner] {
                            width: 440px !important;
                            height: 220px !important;
                        }
            
                        img[class=colimg2] {
                            width: 440px !important;
                            height: 220px !important;
                        }
                    }
            
                    @media only screen and (max-width: 480px) {
            
                        a[href^="tel"],
                        a[href^="sms"] {
                            text-decoration: none;
                            color: #0a8cce;
                            pointer-events: none;
                            cursor: default;
                        }
            
                        .mobile_link a[href^="tel"],
                        .mobile_link a[href^="sms"] {
                            text-decoration: default;
                            color: #0a8cce !important;
                            pointer-events: auto;
                            cursor: default;
                        }
            
                        table[class=devicewidth] {
                            width: 280px !important;
                            text-align: center !important;
                        }
            
                        table[class=devicewidthinner] {
                            width: 260px !important;
                            text-align: center !important;
                        }
            
                        img[class=banner] {
                            width: 280px !important;
                            height: 140px !important;
                        }
            
                        img[class=colimg2] {
                            width: 280px !important;
                            height: 140px !important;
                        }
            
                        td[class=mobile-hide] {
                            display: none !important;
                        }
            
                        td[class="padding-bottom25"] {
                            padding-bottom: 25px !important;
                        }
                    }
                </style>
            </head>
            <body style="background-color: #fff; ">
                <table cellpadding="0" cellspacing="0" border="0" background-color="#fff" align="center" st-sortable="preheader" width="100%" id="background_table">
                <tr>
                    <td style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; "
                        colspan="1" height="10px">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px; "
                            class="device-width">
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                        </table>
                    </td>
                </tr>      
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px;" class="device-width">
                            <tr>
                                <td>
                                    <blockquote style="margin: 0; padding: 0;">
                                        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; border: 1px solid #848f99; font-family:  Helvetica, Arial, sans-serif; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; ">
                                            <tr>
                                                <td class="soft-td-spacer" style="width: 45px; display: block; ">&nbsp;</td>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr><td style="padding-bottom:30px"></td></tr>
                                                        <tr><td style="color: #9ca6af; font-size: 24px; font-family: Helvetica, Arial, sans-serif; "> '. $title .' </td></tr>
                                                        <tr><td style="padding-bottom:30px"></td></tr>
                                                        <tr><td style=" font-size: 14px; line-height: 20px; font-family: Helvetica, Arial, sans-serif; "> '. $body .'  </td></tr>
                                                        <tr><td style="padding-bottom:30px"></td></tr>
                                                        <tr class="line-row"><td style="width: 100%; align: center; height: 1px; background: #e0e6e8; "></td></tr>
                                                        <tr><td style="padding-bottom:30px"></td></tr>
                                                        <tr>
                                                            <td style=" font-size: 14px; line-height: 20px; font-family: Helvetica, Arial, sans-serif; ">
                                                                <label style="font-weight: 400; font-size: 1.5rem;">
                                                                    <b>Flight Operations Crew Information System | FOCIS  </b>
                                                                </label> <br>
                                                                <span style="font-weight: normal; font-size: 14px; color: #9ca6af; "> Please do not reply to this auto generated email message.</span>
                                                                <br><br>  
                                                                <br>Developed by <a target="_blank" href= "https://chamwings.com">  CHAM WINGS </a> - <a target="_blank" href= "https://ITHD.chamwings.com"> IT  Directorate </a>
                                                            </td>
                                                        </tr>';
                                                    $message .='</table>
                                                </td>
                                                <td class="soft-td-spacer" style="width: 45px; display: block; ">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; " colspan="3" height="35">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </blockquote>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; max-width: 600px; " class="device-width">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
            </body>
        </html>';
        $subject1 = 'FOCIS | '. $subject . "\r\n";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "X-Priority: 1 (Highest)\n";
        $headers .= "X-MSMail-Priority: High\n";
        $headers .= "Importance: High\n";
        // More headers
        $headers .= 'From: <focis@chamwings.com>' . "\r\n";
        if($cc != "")
        {
            $headers .= 'Cc:'.$cc . "\r\n";
        }
        if($bcc != "")
        {
            $headers .= 'Bcc:'.$bcc . "\r\n";
        }


        mail($to,$subject1,$message,$headers);
     
    }

    public static function SendEmailToNewEmployee($code, $pass, $email, $cc, $bcc )
    {
       
        $body ='

         Dear,
         <br>
         <br>

        <b>Please note that you have a new account on <u>FOCIS</u> Flight Operations Crew Information System.</b>
        <br><br>
        
        Please find out your own credential <b>(Sign in info)</b> below:
        <br><br>

        <b><u>URL:</u></b> &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;   <a href= "https://focis.chamwings.com">https://focis.chamwings.com</a>
        <br><br>

        <b><u>Credential info:</u></b>
        <br><br>

        Username: <b>'.$email.' </b> <u>OR</u><b> '. $code .'</b> <br> <br>
        Password: <b>'.$pass .' </b>
        <br><br>

        Once you sign in, you must reset the password by click on the link ‘<b>Reset Password</b>’ located in dropDown list in header. 
        
        <br><br><br>


        <b>MIS</b> Department - <b>IS</b> Directorate

        <br>
        <hr>
        <br>


        <div align="right" dir="rtl" >


        عزيزي,
         <br>
         <br>

        <b>لديك حساب جديد على التطبيق <b>FOCIS</b> </b>
        <br><br>

        يرجى الاطلاع على بيانات الاعتماد الخاصة بك <b> (تسجيل الدخول) </b> أدناه:

        <br><br>

        <b> <u>عنوان الموقع :</u> </b> &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; <a href= "https://focis.chamwings.com">https://focis.chamwings.com</a>

        <br><br>

        <b><u>معلومات الاعتماد:</u></b>
        <br><br>

        <b>اسم المستخدم: </b> <span dir="ltr">  <b>'.$email.' </b> <u> أو </u>  <b> ' . $code . '</b> </span> <br>

        <b>كلمه المرور:</b>    <b>'.$pass.' </b>

        <br><br>

        بمجرد تسجيل الدخول ، يجب عليك إعادة تعيين كلمة المرور على الرابط المعنون  ‘<b>Reset Password</b>’ الموجود في القائمة المنسدلة في رأس الصفحة.
        <br><br><br>

        قسم نظم المعلومات الإدارية - إدارة النظم والمعلومات

        <br><br>
        </div>

            ';

            // return Response($body);
        // echo  $email ." - " . $pass . '<br>';

        
        MailController::Mail( 'FOCIS New Account', 'Welcome', $email, $cc, $bcc, $body);
        
    }

}
