<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Rank;
use Illuminate\Http\Request;
use DataTables;
use Response;
use Auth;
use Hash;
// use Excel;   `
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Image;
use Excel;


class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Role = Role::all();
        if (auth::user()->role_id == 1){
            return view('reports', compact('Rank', 'Role'));
        }
        else {
            return abort('404');
        }
        
    }

  
}
