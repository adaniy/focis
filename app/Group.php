<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $guarded = [];
    
    public function User()
    {
        return $this->belongsToMany('App\User','user_group','group_id', 'user_id');
    }

    public function File()
    {
        return $this->belongsToMany('App\Files','group_file','group_id', 'file_id');
    }

}
