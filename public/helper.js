function ResetPassword(user_id, username) {
  $('#pass_user_message').html('');
  $('#pass_user_error').html('');
  $('#new_password').val('');
  $('#new_password_confirmation').val('');
  $('#pass_user_id').val(user_id);
  $('#pass_username').text(username);
  $('#ResetPasswordModal').modal('show');
}

function ResetPassword2(user_id) {
  $('#pass2_user_message').html('');
  $('#pass2_user_error').html('');
  $('#new2_password').val('');
  $('#new2_password_confirmation').val('');
  $('#pass2_user_id').val(user_id);
  $('#ResetPassword2Modal').modal('show');
}

function DownloadFile(fileName, fileType, filePath) {

  temp = fileName.split('.').slice(0, -1).join('.');
  // console.log(temp !== '')
  if (temp) {
    fileName = temp;
  }
  NewFileName = fileName + '.' + fileType;
  console.log(fileName);

  var link = document.createElement('a');
  link.href = filePath;
  link.download = NewFileName;
  link.click();
}

function ShowAlert(type, message) {
  $('.message').text(message);
  alert = '.alert-' + type;
  console.log(alert);

  $(alert).addClass('show');
  setTimeout(function () {
    $(alert).removeClass('show');
  }, 2000)
}


function CheckGroupPermissions(Arr, Group) {
  for (k = 0; k < Arr.length; k++) {
    if (Arr[k].pivot.group_id == Group.id)
      return true;
  }
  return false;
}

function FileGroupsFunction(id) {
  $('.FilePermissionsLoader').fadeIn();
  $('#FilePerModal').modal('show');
  $('#PermissionFileID').val(id)
  $.ajax({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    url: "/FileGroups/" + id,
    type: 'post',
    success: function (data) {
      if (data != null) {
        // {{-- console.log(data); --}}
        str = ``;
        str += `
         
              
              <table class="table table-hover table-sm table-bordered table-striped " style="font-size: 13px;" width="100%"
                cellspacing="0">
                <thead>
                  <tr>
                    <th width="40%"> Files / Groups </th>`;
        for (i = 0; i < data.Groups.length; i++) {
          str += `<th class="center bg-success-ligth text-success">` + data.Groups[i].name + `</th>`
        }
        str += `
                  </tr>
                </thead>
                <tbody>`;

        for (j = 0; j < data.Files.length; j++) {
          str += `<tr>`;
          str += `<th class="bg-secondary-ligth text-danger">
                      <a class="text-primary" target="_blank" href="/file/`+ + data.Files[j].id + `">` + data.Files[j].title + `</a></th>`
          for (i = 0; i < data.Groups.length; i++) {
            if (data.Files[j].group.length > 0) {
              flag = CheckGroupPermissions(data.Files[j].group, data.Groups[i]);
              str += `<th class="center"> `;
              if (flag)
                str += `
                                    <input type="checkbox" name="checkbox[`+ data.Files[j].id + `][]" value="` + data.Groups[i].id + `" checked> `;
              else
                str += `<input type="checkbox" name="checkbox[` + data.Files[j].id + `][]" value="` + data.Groups[i].id + `"> `
              str += `</th>`;
            }
            else
              str += `<th class="center"><input type="checkbox" name="checkbox[` + data.Files[j].id + `][]" value="` + data.Groups[i].id + `">  
                          </th>`;
          }
          str += `</tr>`;
        }
        str += `
    
                </tbody>
              </table>
           
          `;

        $('#FileGroupsDiv').html(str);




      }

    },
    complete: function () {
      $('.FilePermissionsLoader').fadeOut();
    }
  });
}
