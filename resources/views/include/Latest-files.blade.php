    <div class="row">
        <div class="table-responsive">
            <table id="Latest_files" width="100%" style="font-size: 13px;" class="table table-sm table-hover" >
                <thead>
                    <th width="10%">Document added on</th>
                    <th width="60%">Document Name</th>
                    <th width="15%" >More</th>
                    <th></th>
                </thead>
                <tbody >
                    @if ((count($Files) > 0 && $Files != null) )
                        @foreach ($Files as $item)
                        {{--  @dd($Files)  --}}
                            @if ($item->is_read == 0 )
                                <tr class="cyan lighten-5">
                            @else 
                                <tr>
                            @endif
                            <th style="font-size: 14px;" >{{ Carbon\Carbon::parse($item->created_at)->format('Y-m-d') }}</th>

                            {{-- @if($item->file_type == 'image') 
                                <th style="font-size: 14px;"  data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}" class="context-menu-files text-dark pointer">
                                    <a href="{{$item->src}}" target="_blank"> <i class="far fa-image fa-lg text-primary  "></i> {{$item->title}} </a>
                                   
                                    
                                </th>
                            @else 
                                <th style="font-size: 14px;"  data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}" class="context-menu-files text-dark pointer">
                                    <a href="{{$item->src}} " target="_blank" > <i class="far fa-file-pdf fa-lg text-danger "></i> {{$item->title}} </a>
                                    @if ($item->is_read != 1 )
                                        <span class="ml-1 badge badge-danger">New</span> 
                                    @endif

                                </th>
                            @endif --}}

                            <th> {{$item->title}} </th>
                            
                                
                            {{-- <th> <a class="text-primary" href="/Manuals/{{$item->category_id}}/{{$item->id}}" >Details</span> </th> --}}
                                <th> <a class="text-primary" href="/file/{{$item->id}}" >Details</span> </th>

                               
                            <th> 
                                @if ($item->is_read != 1 )
                                <span class="ml-1 badge badge-danger">New</span> 
                                @endif
                            </th>
                                
                                
                                {{--  <td style="font-size: 14px;" >{{$item->file_size}}</td>

                                
                                @if($item->file_type == 'image') 
                                <td style="font-size: 14px;"  data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}" class="context-menu-files text-dark pointer">
                                    <a href="{{$item->src}}" download="{{$item->title}}" target="_blank"> <i class="fa fa-download text-primary" title="Download" aria-hidden="true"></i>  </a>
                                </td>
                                @else 
                                <td style="font-size: 14px;"  data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}" class="context-menu-files text-dark pointer">
                                    <a href="{{$item->src}} " download="{{$item->title}}" target="_blank" > <i class="fa fa-download text-primary" title="Download" aria-hidden="true"></i> </a>
                                </td> 
                                @endif --}}
                                


                           

                                {{--  <td>
                                    <div class="custom-control custom-checkbox">
                                        @if ($item->is_read == 1 )
                                            <input type="checkbox" checked disabled class="mark_as_read" name="mark_as_read[]" value="{{$item->id}}" >
                                        @else 
                                            <input type="checkbox"  class="mark_as_read" name="mark_as_read[]" value="{{$item->id}}" >
                                        @endif
                                        
                                    </div>
                                </td>  --}}
                            
                            </tr>
                        @endforeach
                   
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $('#Latest_files').DataTable({
                responsive: !0,
                processing: true,
                sort : false,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false
            });
        });
        
    </script>
