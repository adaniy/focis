<div class="row">
    @if ((count($category) > 0 && $category != null) || (count($Files) > 0 && $Files != null) )
        @foreach ($category as $item)
            @if($item->id != 0)
                <div class="col-md-2 file mb-2">
                    <div value=" {{$item->id}}" style="height: 100%;" onclick="ShowCategory('grid', {{$item->id}},'{{$item->name}}')" data-id="{{$item->id}}" data-name="{{$item->name}}" class="card hoverable context-menu-folder pointer">
                    <div class="card-body 1my-4">
                        <i class="far fa-folder-open fa-lg text-warning"></i>
                        <p style="font-size: 14px;">
                        <span class="text-dark"> {{$item->name}} </span>
                        <br>
                        <span class="small text-dark"></span>
                        </p>
                    </div>
                    </div>
                    <br>
                </div>
            @endif
        @endforeach

        @foreach ($Files as $item)
            <div class="col-md-2 file mb-2">
                <a href="{{$item->src}}" target="_blank" style="height: 100%;" data-url= "{{$item->src}}" data-id="{{$item->id}}" data-name="{{$item->title}}" class="card hoverable context-menu-files pointer">
                    @if ($item->is_read == 0 )
                        <div class="card-body 1my-4 cyan lighten-5 " style="margin-bottom: -20%; max-height: 100%;">
                    @else 
                        <div class="card-body 1my-4 " style="margin-bottom: -20%;    max-height: 100%;">
                    @endif
                    @if($item->file_type == 'image') 
                        <i class="far fa-image fa-lg "></i>
                    @else 
                        <i class="far fa-file-pdf fa-lg text-danger"></i>
                    @endif
                    <p style="font-size: 14px;">
                    <span class="text-dark"> {{$item->title}} </span>
                    <br>
                    <span class="small text-primary">{{$item->file_size}} </span>
                    </p>
                </div>
                </a>
                <br>
            </div>
        @endforeach
        
    @else 
    <div class="container">
        <br>
        <img width="10%" class="img-fluid" src="/img/empty.png"> 
        <h4 class="u-font-strong empty-folder-table-title">This folder is empty</h4>
    </div>
    @endif
</div>

