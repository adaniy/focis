<style>
  select {
    border: 0px !important;
    border-bottom: 1px solid #ced4da !important;
  }

  select:focus:not([readonly]) {
    border: 0px;
    border-bottom: 2px solid #4258f4 !important;
    box-shadow: 0 1px 0 0 solid #4258f4 !important;
  }

  .select-lable {
    padding-bottom: 15px !important;
    font-size: 11px !important;
  }

  .md-form {
    margin-top: 1rem;
    margin-bottom: 0.25rem;
  }
</style>


<div class="modal fade" id="NewUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  data-keyboard="false" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-fluid">
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> <i class="fas fa-plus mr-1"></i> New
          User </span> </div>
      <div class="card-body">
        <div class="row">
          <!-- Second column -->
          <div class="col-lg-12 mb-4">
            <form id="NewUserForm">
              <!-- First row -->
              @csrf

              <div class="row">

                <div class="col-md-4">
                  <div class="md-form mb-0">
                    <input type="text" maxlength="3" name="new_code" id="new_code" class="form-control validate">
                    <label for="new_code" data-error="wrong" data-success="right" class="active">3 Letter Code</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="md-form mb-0">
                    <input type="text" name="new_name" id="new_name" class="form-control validate">
                    <label for="new_name" data-error="wrong" data-success="right">Name</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="md-form mb-0">
                    <input type="email" name="new_email" id="new_email" class="form-control validate">
                    <label for="new_email" data-error="wrong" data-success="right">Email Address</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="md-form mb-0">
                    <label for="new_role_id" data-error="wrong" data-success="right">Select Role</label><br>
                    <select name="new_role_id" id="new_role_id" class="form-control form-control-sm mb-2">
                      <option disabled selected> -- Select Role -- </option>
                      @foreach ($Role as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="md-form mb-0">
                    <label for="new_rank_id" class="mdb-main-label" data-error="wrong" data-success="right">Select
                      Rank</label><br>
                    <select name="new_rank_id" id="new_rank_id" class="form-control form-control-sm mb-2">
                      <option disabled selected> -- Select Rank -- </option>
                      @foreach ($Rank as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>


                  </div>
                </div>

              </div>
              <div class="row">

                <!-- First column -->
                <div class="col-md-12">
                  <div class="md-form mb-1">
                    <textarea type="text" name="new_note" id="new_note" class="md-textarea form-control"
                      rows="1"></textarea>
                    <label for="new_note">Note</label>
                  </div>
                </div>
              </div>
              <!-- Third row -->

              <!-- Fourth row -->
              <div class="row">
                <div class="col-md-12 text-center my-4">
                  <span class="waves-input-wrapper waves-effect waves-light"><input type="submit" value="Save"
                      class="btn btn-info btn-rounded"></span>
                  <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4"
                    data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- Fourth row -->

            </form>
          </div>
          <!-- Second column -->

        </div>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="EditUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  data-keyboard="false" data-backdrop="static" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content modal-fluid">
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> <i class="fas fa-edit mr-1"></i> Edit
          User </span> </div>
      <div class="card-body">
        <div class="row">
          <!-- Second column -->
          <div class="col-lg-12 mb-4">
            <form id="EditUserForm">
              <!-- First row -->
              @csrf
              <input type="hidden" name="edit_user_id" id="edit_user_id">
              <div class="row">

                <div class="col-md-4">
                  <div class="md-form mb-0">
                    <input type="text" maxlength="3" name="edit_code" id="edit_code" class="form-control validate">
                    <label for="edit_code" data-error="wrong" data-success="right" class="active">3 Letter Code</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="md-form mb-0">
                    <input type="text" name="edit_name" id="edit_name" class="form-control validate">
                    <label for="edit_name" data-error="wrong" data-success="right">Name</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="md-form mb-0">
                    <input type="email" name="edit_email" id="edit_email" class="form-control validate">
                    <label for="edit_email" data-error="wrong" data-success="right">Email Address</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="md-form mb-0">
                    <label for="edit_role_id" data-error="wrong" data-success="right">Select Role</label><br>
                    <select name="edit_role_id" id="edit_role_id" class="form-control form-control-sm mb-2">
                      <option disabled selected> -- Select Role -- </option>
                      @foreach ($Role as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="md-form mb-0">
                    <label for="edit_rank_id" data-error="wrong" data-success="right">Select Rank</label><br>
                    <select name="edit_rank_id" id="edit_rank_id" class="form-control form-control-sm mb-2">
                      <option disabled selected> -- Select Rank -- </option>
                      @foreach ($Rank as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>


                  </div>
                </div>

              </div>
              <div class="row">

                <!-- First column -->
                <div class="col-md-12">
                  <div class="md-form mb-1">
                    <textarea type="text" name="edit_note" id="edit_note" class="md-textarea form-control"
                      rows="1"></textarea>
                    <label for="edit_note">Note</label>
                  </div>
                </div>
              </div>
              <!-- Third row -->

              <!-- Fourth row -->
              <div class="row">
                <div class="col-md-12 text-center my-4">
                  <span class="waves-input-wrapper waves-effect waves-light"><input type="submit" value="Save"
                      class="btn btn-info btn-rounded"></span>
                  <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4"
                    data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- Fourth row -->

            </form>
          </div>
          <!-- Second column -->

        </div>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="ActivateModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"
  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-fluid">
      <div class=" md-tabs tabs-2 light-blue darken-3"> <span class="text-white"> <i class="fas fa-edit mr-1"></i>
          Account Status and Permissions </span> </div>
      <div class="card-body">
        <div class="row">
          <!-- Second column -->
          <div class="col-lg-12 mb-4">
            <form id="ActiveForm">
              @csrf
              <input class="active_user_id" name="active_user_id" type="hidden">
              <span class="my-4 pb-4"><strong id="active_username"></strong> </span>
              <div class="custom-control custom-checkbox">
                <input type="hidden" value="0" name="is_active">
                <input type="checkbox" class="custom-control-input" value="1" name="is_active" id="is_active">
                <label class="custom-control-label" for="is_active">Active Account </label>
              </div>

              <div class="custom-control custom-checkbox">
                <input type="hidden" value="0" name="is_Authorized">
                <input type="checkbox" class="custom-control-input" value="1" name="is_Authorized" id="is_Authorized">
                <label class="custom-control-label" for="is_Authorized">Authorize Account</label>
              </div>

              <!-- Fourth row -->
              <div class="row">
                <div class="col-md-12 text-center my-2">
                  <span class="waves-input-wrapper waves-effect waves-light"><input type="submit" value="Save"
                      class="btn btn-info btn-rounded"></span>
                  <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4"
                    data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- Fourth row -->

            </form>
          </div>
          <!-- Second column -->

        </div>
      </div>
    </div>

  </div>
</div>



<div class="col-md-12">
  <div class="form-group">
    <button type="button" class="btn btn-sm1 btn-primary" data-toggle="modal" data-target="#NewUserModal">New
      User</button>
  </div>

  <div class="table-responsive">
    <table class="table table-hover table-sm " style="font-size: 14px;" id="Users" width="100%" cellspacing="0">
      <thead>
        <tr>
          <th> Code </th>
          <th> Name </th>
          <th> Email </th>
          <th> Role </th>
          <th> Rank </th>
          <th> Groups </th>
          <th> Active </th>
          <th> Authorized </th>
          <th width="15%"> Action </th>
        </tr>

      </thead>
      <tbody>
      </tbody>
    </table>
  </div>

  <div class="LatestFiles"></div>
</div>

<script>
  var Users;
    $(document).ready(function() {
      
      $("select").focus(function() { 
          var label = $("label[for='" + $(this).attr('id') + "']");
          label.addClass('active');
          label.removeClass('select-lable')
      });
      $("select").focusout(function(){
          var label = $("label[for='" + $(this).attr('id') + "']");
          label.removeClass('active');
          label.addClass('select-lable');
      }); 
      Users = $('#Users').DataTable({
        orderCellsTop: true,
        responsive: !0,
        processing: true,
        serverSide: false,
        ajax: {
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type: 'post',
            data:function(){
                {{--  return data = {'from':$('#IR_from_date').val(),'to':$('#IR_to_date').val(),'status':$('#IR_status').val() };  --}}
            },
        url :'/GetUsers',
        },
       
        columns: [
            {
                data: function (data) {
                    return   data.code ;
                },
                name: 'code'
            },
            {
                data: function (data) {
                    return '<strong>' + data.name + '</strong>'  ;
                },
                name: 'name'
            },
            {
                data: function (data) {
                    return '<a href="mailto:' + data.email + '" class="text-info" >' + data.email + '</span>'  ;
                },
                name: 'email'
            },
            {
                data: function (data) {
                  if(data.role != null){
                    return   '<span>' + data.role.name + '</span> ';
                  }
                  else {
                    return   '--';
                  }
                },
                name: 'role.name'
            },

            {
              data: function (data) {
                if(data.rank != null){
                  return   '<span>' + data.rank.name + '</span> ';
                }
                else {
                  return   '--';
                }
              },
              name: 'rank.name'
          },

            {
                data: function (data) {
                    return   '--';
                },
                name: 'created_at'
            },
            {
              data: function (data) {
                  if (data.is_active == 1){
                    return   '<span class="text-success">Yes</span> ';
                  }
                  else {
                    return   '<span class="text-danger">No</span> ';  
                  }
                  
              },
              name: 'created_at'
            },
            {
              data: function (data) {
                  if (data.is_authorized == 1){
                    return   '<span class="text-success">Yes</span> ';
                  }
                  else {
                    return   '<span class="text-danger">No</span> ';  
                  }
                  
              },
              name: 'created_at'
            },
            {
                data: function (data) {
                  str = '';

                    str+= `
                    
                        <i onclick="ShowUser(` + data.id + `) " data-toggle="tooltip" data-placement="top" title="Show User" class="fa fa-eye text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                        <i onclick="EditUser(` + data.id + `) " data-toggle="tooltip" data-placement="top" title="Edit User" class="fa fa-edit text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                        <i onclick="ResetPassword(` + data.id + `, '`  + data.name + `')" data-toggle="tooltip" data-placement="top" title="Reset Password" class="fa fa-key text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                        <i onclick="ActiveUser(` + data.id + `, '`  + data.name + `' , ` + data.is_active + `, ` + data.is_authorized + ` ) " data-toggle="tooltip" data-placement="top" title="Active & Authorize Account" class="fa fa-lock text-secondary pointer"> </i> <span style="padding-right:10px"></span>
                        <i style="display: none" class="UserLoader`+ data.id +` fas fa-spinner fa-lg1 text-success fa-spin fa-fw"></i>`;

                        return str;
                },
                name: 'id'
            },
        ],
        {{--  <i onclick="ShowUser(` + data.id + `) " data-toggle="tooltip" data-placement="top" title="Show User info" class="fas fa-eye text-secondary pointer"> </i> <span style="padding-right:10px"></span>  --}}
      });

      {{-- $('#IR_Serach').trigger('click'); --}}
      $('#IR_Serach').click(function()
      {
        Users.DataTable().ajax.reload();
          
      })
    });

   

    $('#NewUserForm').on('submit', function(e){
      e.preventDefault();
      var data = $(this).serializeArray();
      $.ajax({
          url: '/StoreUser',
          data: data,
          type: 'post',
          success: function(data) {
              // console.log(data.status);
            if (data.status == 'success') {
              $('#Users').DataTable().ajax.reload();
              $('#NewUserModal').modal('hide');
            } else {
              $('#NewUserModal').modal('hide');
            }
          },
          complete: function() {},
      })
    });

    $('#EditUserForm').on('submit', function(e){
      e.preventDefault();
      var data = $(this).serializeArray();
      // console.log(data)
      $.ajax({
          url: '/UpdateUser',
          data: data,
          type: 'post',
          success: function(data) {
              // console.log(data.status);
          if (data.status == 'success') {
            $('#Users').DataTable().ajax.reload();
              $('#EditUserModal').modal('hide');
          } else {
              $('#EditUserModal').modal('hide');
          }
          },
          complete: function() {},
      })
  
    })

    $('#DeactiveForm').on('submit', function(e){
      e.preventDefault();
      var data = $(this).serializeArray();
      ActiveUserFun(data);
    });

    $('#ActiveForm').on('submit', function(e){
      e.preventDefault();
      var data = $(this).serializeArray();
      ActiveUserFun(data);
    });

    function ActiveUserFun(data)
    {
      $.ajax({
        url: '/ActiveUser',
        data: data,
        type: 'post',
        success: function(data) {
          // console.log(data.status);
          if (data.status == 'success') {
            $('#Users').DataTable().ajax.reload();
              $('#DeactivateModal').modal('hide');
              $('#ActivateModal').modal('hide');
          } else {
            $('#DeactivateModal').modal('hide');
            $('#ActivateModal').modal('hide');
          }
        },
        complete: function() {},
      })
    }
  
    function ShowUser(id) {
      $('.UserLoader'+id).fadeIn();
      $.ajax({
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          url: "/ShowUser/" + id,
          type: 'post',
          success: function(response) {
            if(response.status == 'success'){
              // console.log(response);
              $('#show_code').text(  (response.user.code).toUpperCase() );
              $('#show_name').text(response.user.name);
              $('#show_email').text(response.user.email);
              $('#show_note').text(response.user.note);
              $('#show_role').text(response.user.role.name);
              $('#show_rank').text((response.user.rank != null ? response.user.rank.name : '--'));
              $('#show_active').html((response.user.is_active == 1 ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>'  ));
              $('#show_authorized').html((response.user.is_authorized == 1 ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>'  ));
              $('#show_note').text((response.user.note != null ? response.user.note : ''  ));

              str =``;
              str +=`
              <div class="table-responsive1">
                  <table class="table table-hover table-sm table-striped " style="font-size: 12px; " id="ShowUserGroupsTable" width="100%"
                    cellspacing="0">
                    <thead>
                      <tr>
                        <th width="5%"> # </th>
                        <th >Group Name </th>
                      </tr>
                    </thead>
                    <tbody>`;

                      c = 1;
                      if (response.user.group.length > 0){
                        for( i = 0; i < response.user.group.length; i++ )
                        {
                          str +=`
                          <tr>
                            <th class="text-dark">` + c++  + `</th>
                            <th class="text-dark">` + response.user.group[i].name  + `</th>
                          </tr>`;
                        }
                      }
                      else {
                        str +=`
                          <tr>
                            <th class="center" colspan="2">No data available in tables</th>
                            
                          </tr>`;
                      }
                      
                      str += `
                    </tbody>
                  </table>
              </div>
              `;

              $('#ShowUserGroupsDiv').html(str);

            }
          
          },
            complete: function() {
              $('.UserLoader'+id).fadeOut();
              $('#ShowUserModal').modal('show');
          }
      });
    }

    function EditUser(id) {
      $('.UserLoader'+id).fadeIn();
      $('#edit_user_id').val(id);
      // console.log(id);
      $.ajax({
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          url: "/ShowUser/" + id,
          type: 'post',
          success: function(response) {
            if(response.status == 'success'){
              // console.log(response);
              $('#edit_code').val(response.user.code).siblings('label').addClass('active');
              $('#edit_name').val(response.user.name).siblings('label').addClass('active');
              $('#edit_email').val(response.user.email).siblings('label').addClass('active');
              $('#edit_note').val(response.user.note).siblings('label').addClass('active');
              $('#edit_role_id').val(response.user.role_id).trigger('change');
              $('#edit_rank_id').val(response.user.rank_id).trigger('change');
            }
          
          },
            complete: function() {
              $('.UserLoader'+id).fadeOut();
              $('#EditUserModal').modal('show');
          }
      });
    }

    
    function ActiveUser(id, name, active, authorized ) {
      console.log(active, authorized)
      if(active == 1){
      console.log(active == 1 , 1)

        $('#is_active').prop("checked", true);
      }
      else{
        console.log(active == 0 ,0)
        $('#is_active').prop("checked", false);
      }
      if(authorized == 1){
        console.log(authorized == 1, 1)
        $('#is_Authorized').prop("checked", true);
      }
      else
      {
        console.log(authorized == 0 , 0)
        $('#is_Authorized').prop("checked", false);
      }

      {{--  $('.UserLoader'+id).fadeIn();
      $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: "/ShowUser/" + id,
        type: 'post',
        success: function(response) {
          if(response.status == 'success'){
             if(response.user.is_active == 1){
              $('#is_active').attr("checked", true);
            }
            else{
              $('#is_active').attr("checked", false);
            }
            if(response.user.is_authorized == 1){
              $('#is_Authorized').attr("checked", "checked");
            }
            else
            {
              $('#is_Authorized').attr("checked", false);
            }
          }

        
        },
          complete: function() {
            $('.UserLoader'+id).fadeOut();
            $('#EditUserModal').modal('show');
        }
      });  --}}

       
        $('#active_username').text(name);
        $('#ActiveForm .active_user_id').val(id);
        $('#ActivateModal').modal('show');
    }

</script>