<script>
    $(".mark_as_read").on("click", function(){
        {{--  $(".mark_as_read").click(function () {  --}}
        $('#confirm').unbind('click');
        $('#cancel').unbind('click');
        // console.log($(this).is(":checked"));
        This = $(this);
        if ($(this).is(":checked")){
            isRead = 1;
        } 
        else 
            isRead = 0;
    
        $('#modalConfirm').modal('show');
        $('#confirm').click(function(){
            id = This.val();
            // console.log(id);
            $.ajax({
                url: '/MarkFileAsRead',
                type: 'get',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: {'id': id , 'isRead': isRead  },
                success: function (data) {
                    if (data.status == 'success'){
                        ShowAlert('success', 'Data Updated successfully');
                        $('#modalConfirm').modal('hide');
                        $('#root_path li:last-child').trigger('click');
                        ShoFile();
                    }
                    else {
                        $('#root_path li:last-child').trigger('click');
                        ShowAlert('danger', 'sorry, something went wrong');
                        ShoFile();
                        
                    }
                },
                complete: function(){
                    $('.ResourceLoaderNote').fadeOut();
                }
            });
        });
    
        $('#cancel').click(function(){
            This.prop('checked', false); 
        });
    });

    $(".confirm_reading").on("click", function(){
        {{-- console.log($(this).val()); --}}
        id = ($(this).val());
        if ($(this).is(":checked"))
            Confirm = 1;
        else 
            Confirm = 0;

        $.ajax({
            url: '/ConfirmReading',
            type: 'get',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {'id': id , 'Confirm': Confirm  },
            success: function (data) {
                if (data.status == 'success'){ 
                    $('#root_path li:last-child').trigger('click');

                    ShowAlert('success', 'Data Updated successfully');
                }
                else {
                $('#root_path li:last-child').trigger('click');
                ShowAlert('danger', 'sorry, something went wrong');
                }
            },
            complete: function(){
                
            }
        });


    
        {{-- $('#modalConfirm').modal('show'); --}}
        $('#confirm').click(function(){
            id = This.val();
            // console.log(id);
        
        });
    
        $('#cancel').click(function(){
            This.prop('checked', false); 
        });
    });
    
   
            
</script>