@extends('master.app')
@section('title' , ':: Manuals - Flight Operations ::')

@section('content')
@include('master.header')

  
  @include('Files.modals')

  <style>
    .dz-success-mark svg{
      border-radius: 50%;
      background: #28a745;
    }

    .dz-error-mark svg{
      border-radius: 50%;
      background: #dc3545;
    }
    .dropzone .dz-preview .dz-remove {
      position: absolute;
      top: -20%;
      left: 20%;
      
    }
    
  </style>
<main class="mt-5 pt-5">
  <div class="container1">

    <div class="row">
      <div class="col-md-2">
        
        @include('include.sidebar')
      </div>

      <div class="col-md-10">
        <h4 class="text-center font-weight-bold mb-3"> Manuals & Forms </h4> 
          <div class="col-md-12">
            <nav aria-label="breadcrumb">
              <div class="row">
                <div class="col-lg-3 col-md-12 "></div>
                 
                    <div class="col-lg-9 center1">
                      <div role="group" class="btn-group">
                        <button type="button" onclick="reOpenPath('#root_path li',0,'Home');" title="Home" class="btn btn-sm btn-info">
                          <i class="fas fa-home"></i>
                          {{-- <i class="fas fa-sync-alt"></i> --}}
                        </button>
                        <button type="button" title="Up One Level" id="UpOneLevel" class="btn btn-sm btn-info"><i class="fas fa-level-up-alt"></i></button>
                      </div>
                      @if(Auth::user()->role_id == 1)
                        <div role="group" class="btn-group">
                          <button type="button" title="New folder" data-toggle="modal" data-target="#NewFolderModal" id="NewFolderModal1" class="btn btn-sm btn-info"><i class="far fa-folder"></i text-warning></button>
                          <button type="button" title="Upload" data-toggle="modal" data-target="#UploadFileModal" id="UploadFileModal1" class="btn btn-sm btn-info"><i class="fas fa-upload"></i></button>
                        </div>
                      @endif
                        <div role="group" class="btn-group">
                          <button type="button" title="List" onclick="ShowCategoryL('list',0,'Home');" id="GridCategory" class="btn btn-sm btn-info "><i class="fas fa-th-list"></i></button>
                          <button role="button" title="Grid" onclick="ShowCategoryG('grid',0,'Home');" id="ListCategory" class="btn btn-sm btn-info "><i class="fas fa-th"></i></button>
                        </div>
                    
                      <div role="group" class="btn-group">
                        <button  onclick="toggleFullScreen();" type="button" title="Full screen" class="btn btn-sm btn-info requestfullscreen"><i class="fas fa-expand-arrows-alt"></i></button>
                      </div>
                      <div role="group" class="btn-group">
                        <button type="button" title="About" class="btn btn-sm btn-info"><i class="fas fa-question"></i></button>
                      </div>
      
      
                                
                    </div> 
                
              </div>
            </nav>
            <div class="row mb-4 pt-2 wow fadeIn">
              {{--  <div class="col-lg-3 col-md-3 ">
                @include('master.subleft')
              </div>  --}}
              <div class="col-lg-12 col-md-12 ">
              
                <div id="subright">
                  <nav aria-label="breadcrumb" >
                    <ol class="breadcrumb pointer" style="height: 30px;" id="root_path">{!! $parint !!}</ol>
                  </nav>
                    <div class="border1" id="subright1" > 
                      <section class="dark-grey-text text-center" >
                        {{--  @dd(phpinfo())  --}}
                        <div style="font-size: 20px !important" class="container3"></div>
                      </section>
                    </div>
                </div>
              </div>
            </div>
          
      
          </div>
      </div>

    </div>

   
</main>

  <script>
    $(document).ready(function() {
        @if(Auth::user()->role_id == 1)
            $('.context-menu-one').on( "click", function() {
            $(this).toggleClass('item-selected');
            });
        
            $(function(){
            /**************************************************
            * Context-Menu Folder
            **************************************************/
            $.contextMenu({
                selector: '.context-menu-folder', 
                callback: function(key, options) {
                    // console.log(key , $(options.$trigger[0]).attr('data-selector'));
                    var selectorID = $(options.$trigger[0]).attr('data-id');
                    var selectorName = $(options.$trigger[0]).attr('data-name');
                    switch(key) {
                    case 'rename':
                        // console.log(selectorName , selectorID);
                        $('#EditFolderModal').modal('show');
                        $("#edit_catID").val(selectorID);
                        $("#edit_name").val(selectorName).siblings('label').addClass('active');
                        break;
                    case 'delete':
                        $('#DeleteModal').modal('show');
                        $("#deleteType").val('folder');
                        $('#ErrorMessage').css('display', 'none');
                        $("#deleteID").val(selectorID);
                        $("#deleteName").text(selectorName);
                        break;
                    default:
                    }
                    
                },
                items: {
                    "rename": {name: "Rename", icon: "edit"},
                    "sep1": "---------",
                    "delete": {name: "Delete", icon: "delete"},
                    
                }
                
            });
            });
    
    
            $(function(){
            /**************************************************
            * Context-Menu Files
            **************************************************/
            $.contextMenu({
                selector: '.context-menu-files', 
                callback: function(key, options) {
                    // console.log(key , $(options.$trigger[0]).attr('data-selector'));
                    var selectorID = $(options.$trigger[0]).attr('data-id');
                    var selectorName = $(options.$trigger[0]).attr('data-name');
                    var selectorURL = $(options.$trigger[0]).attr('data-url');
                    var selectorType = $(options.$trigger[0]).attr('data-type');
                    
                    switch(key) {
                    case 'rename':
                        // console.log(selectorName , selectorID);
                        $('#EditFileModal').modal('show');
                        $("#edit_fileID").val(selectorID);
                        $("#edit_fileName").val(selectorName);
                        break;
                    case 'delete':
                        $('#DeleteModal').modal('show');
                        $("#deleteType").val('file');
                        $('#ErrorMessage').css('display', 'none');
                        $("#deleteID").val(selectorID);
                        $("#deleteName").text(selectorName);
                        break;
                    case 'filePermissions':
                        FileGroupsFunction(selectorID);
                        
                        break;
                    case 'download':
                    DownloadFile(selectorName , selectorType , selectorURL);
                    default:
                    }
                    
                },
                items: {
                    "rename": {name: "Rename", icon: "edit"},
                    "filePermissions": {name: "File Permissions", icon: "fa-cogs"},
                    "download": {name: "Download", icon: "fa-download"},
                    "sep1": "---------",
                        "delete": {name: "Delete", icon: "delete"                   
                    },
                    
                }
            });
            });
        @endif
    
        $("body").on("contextmenu", "table tr", function(e) {
            $('table tr').css("background-color", ""); //Remove previos color.
            $(this).css("background-color", "#A7DAEB"); //Here Change color to yellow.
            
        });
    
        $(function(){
            $.contextMenu({
            selector: '.context-menu-one',
            items: {
                "textinput": {name: "Type something:", type: 'text', 
                events: {
                    keydown: function(event) {
                    $(this).css({"color":"magenta","font-size":"14px"});
                    //$(this).parent().parent().css({"background":"green","font-size":"20px"});  //works too
                    }
                }
                }
            }
            });
        });
    
        $('html').click(function() {
            $('table tr').css("background-color", "");
        });
    });
            
  </script>

  <script>
    var Gloudal_CategoryType = 'list'; 
    var CurrentPath = 0; 
    $(document).ready(function() {
      $('#new_parent').val(CurrentPath);
      $('.treeview').mdbTreeview();

      @if($cat==0)
        ShowCategory(Gloudal_CategoryType , 0,'Home');
      @else
        ShowCategory(Gloudal_CategoryType , {{$cat[0]}},'{{$cat[1]}}');
      @endif
     
     
      $("#FilePermissionsForm").on('submit',function(event){
        $('#FilePermissionsForm').unbind('click');
        event.preventDefault();
        data1 = $( this ).serializeArray();
          console.log(data1);
          data1 = $( this ).serializeArray();
          $.ajax({
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              url: '/UpdateGroupsFile',
              type: 'post',
              data: data1,
              success: function (data) {
                if (data.status == 'success'){ 
                  $('#FilePerModal').modal('hide');  
                  ShowAlert('success', 'Data Updated successfully');
                }
                else {
                  $('#FilePerModal').modal('hide');  
                  ShowAlert('danger', 'sorry, something went wrong');
                }
              },
              error : function(){
                FilePermissions();
                ShowAlert('danger', 'sorry, something went wrong');
              },
          });
      });
      
    // New Folder Form
      $( "#NewFolderForm" ).submit(function( event ) {
        event.preventDefault();
        $('.Loader').fadeIn();
        console.log(CurrentPath,$('#new_parent').val());
          data1 = $( this ).serializeArray();
          $.ajax({
              url: '/StoreCategory',
              type: 'post',
              data: data1,
              success: function (data) {
              $('#NewFolderModal').modal('hide');

              $('#root_path li:last-child').trigger('click');
              },
              complete: function(){
                $('.ResourceLoader').fadeOut();
              }

            
          });
      });

    // Edit Folder Form
      $( "#EditFolderForm" ).submit(function( event ) {
        event.preventDefault();
        $('.Loader').fadeIn();
        
          data1 = $( this ).serializeArray();
          $.ajax({
              url: '/EditCategory',
              type: 'post',
              data: data1,
              success: function (data) {
              $('#EditFolderModal').modal('hide');
                $('#root_path li:last-child').trigger('click');
              },
              complete: function(){
                $('.ResourceLoader').fadeOut();
              }

            
          });
      });

    // Edit File Form
      $( "#EditFileForm" ).submit(function( event ) {
        event.preventDefault();
        $('.Loader').fadeIn();
        
          data1 = $( this ).serializeArray();
          $.ajax({
              url: '/EditFile',
              type: 'post',
              data: data1,
              success: function (data) {
              $('#EditFileModal').modal('hide');
                $('#root_path li:last-child').trigger('click');
              },
              complete: function(){
                $('.ResourceLoader').fadeOut();
              }

            
          });
      });

    // Delete Form
      $( "#DeleteForm" ).submit(function( event ) {
        event.preventDefault();
        $('.Loader').fadeIn();
          if ($('#deleteType').val() == 'folder'){
            url = '/DeleteCategory';
          }
          else {
            url = '/DeleteFile';
          }
        
          data1 = $( this ).serializeArray();
          $.ajax({
              url: url,
              type: 'post',
              data: data1,
              success: function (data) {
                if (data.status == 'fail'){
                  $('#ErrorMessage').css('display', 'block');
                }
                else {
                $('#ErrorMessage').css('display', 'none');
                $('#DeleteModal').modal('hide');
                $('#root_path li:last-child').trigger('click');
                }
              
              },
              complete: function(){
                $('.ResourceLoader').fadeOut();
              }
          });
      });
 
    });

    $('#UpOneLevel').on('click' , function(){
      root = $('#root_path li');
      if(root.length > 1)
        $(root[root.length-2]).trigger('click')

    });


    function ViewFile(fileName , fileType, filePath){

      console.log(filePath);
      fileName = fileName.split('.').slice(0, -1).join('.'); 
      NewFileName = fileName + '.' + fileType; 


      var link = document.createElement('a');
      link.href = filePath;
      link.download = NewFileName;
      // link.click();
      var win = window.open(filePath, '_blank');
      
    }

    $("#UploadFileModal1").click(function (e) {
      Dropzone.forElement("#my-dropzone").removeAllFiles(true);
    });

    $("#RemoveAllFiles").click(function (e) {
      Dropzone.forElement("#my-dropzone").removeAllFiles(true);
    });

    $("#NewFolderModal1").click(function (e) {
      $("#new_name").val('');
      
    });
    
    function EditFolder(id , name)
    {
      $('#EditFolderModal').modal('show');
      $("#edit_name").val(name);
    }

    function ShowCategory(CategoryType , parent ,name, reopen = false )
    {
      // console.log(CategoryType , parent ,name );
      if (CategoryType == 'grid'){
        $('#ListCategory').addClass('active');
        $('#GridCategory').removeClass('active');
      }
      else {
        Gloudal_CategoryType = 'list';
        $('#GridCategory').addClass('active');
        $('#ListCategory').removeClass('active');
      }
      $('#new_parent').val(parent);
      // console.log(CurrentPath);
      $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/ShowCategory',
        data: {'type' : CategoryType , 'parent' : parent},
        type: 'post',
        success: function (data) {
          $('.container3').html(data.html);
          CurrentPath = parent; 
          {{--  console.log(CurrentPath);  --}}
          $('#root_path .active').removeClass('active');
          if(!reopen){
            $('#root_path').append('<li class="breadcrumb-item small active" onclick="reOpenPath(this, '+ parent +' , `'+ name +'` )">'+ name +'</li>');
          
          }
        },
        complete: function(){
        }
      
      });
    }

    function reOpenPath(This, parent, name )
    {
      // console.log(This , parent ,name );
      $(This).nextAll('li').remove();
      ShowCategory(Gloudal_CategoryType , parent ,name, true );
    }

    function ShowCategoryL(){Gloudal_CategoryType='list'; reOpenPath(null, CurrentPath,'Home')}
    function ShowCategoryG(){Gloudal_CategoryType='grid'; reOpenPath(null, CurrentPath,'Home')}

    {{--  
      Dropzone.prototype.defaultOptions.dictDefaultMessage = "Drop files here to upload";
      Dropzone.prototype.defaultOptions.dictFallbackMessage = "Your browser does not support drag'n'drop file uploads.";
      Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
      Dropzone.prototype.defaultOptions.dictFileTooBig = "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.";
      Dropzone.prototype.defaultOptions.dictInvalidFileType = "You can't upload files of this type.";
      Dropzone.prototype.defaultOptions.dictResponseError = "Server responded with {{statusCode}} code.";
      Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancel upload";
      Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "You can not upload any more files.";
      Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Are you sure you want to cancel this upload?";  
    --}}
    Dropzone.prototype.defaultOptions.dictRemoveFile = "Remove item";
    Dropzone.options.myDropzone = {
      url: "/uploadFiles/" + CurrentPath,
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
      maxFilesize: 209715200, 
      timeout: 1800000,
      autoProcessQueue: false,
      uploadMultiple: false,
      parallelUploads: 10,
      maxFiles: 10,
      acceptedFiles: "image/*,application/pdf",
      addRemoveLinks: true,
      dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
      renameFile: function (file) {
        let newName = (file.name).replace("'", '');
        // console.log(newName);
        return newName;
      },
    
      
      


      init: function () {
        var submitButton = document.querySelector("#submit");
        var wrapperThis = this;
        this.removeAllFiles(true);
        submitButton.addEventListener("click", function () {
          wrapperThis.processQueue();
        });

        this.on("addedfile", function (file) {
          // console.log(CurrentPath)
          this.options.url = "/uploadFiles/" + CurrentPath;
          // Create the remove button
          // var removeButton = Dropzone.createElement("<a style='cursor:pointer' class='text-danger'>Remove File</a>");

          // Listen to the click event
              // removeButton.addEventListener("click", function (e) {
              // Make sure the button click doesn't submit the form:
              // e.preventDefault();
              // e.stopPropagation();

              // Remove the file preview.
              // wrapperThis.removeFile(file);
              // If you want to the delete the file on the server as well,
              // you can do the AJAX request here.
          });


        this.on('completemultiple', function () {
          //
        });

        this.on('successmultiple', function () {
          $('#root_path li:last-child').trigger('click');
        });

        this.on('success', function () {
          $(".dz-error-mark").css("display", "none");
          $('#root_path li:last-child').trigger('click');
        });

        this.on("error", function(file, errormessage, xhr){
            if(xhr) {
                var response = JSON.parse(xhr.responseText);
                // console.log(response.upload_status);
                $('#my-dropzone').addClass("dz-error").find('.dz-error-message').text(response.upload_status);
            }
        });
          
      },
    };
    
    function ShoFile()
    {
      return true;
    }



</script>


@endsection