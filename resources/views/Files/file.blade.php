    <style>
        tr {
            text-align: left !important;
        }
        .dropzone .dz-preview.dz-success .dz-success-mark {
            opacity: 1;
        }
    </style>
@include('Files.JSFile')
<div class="row">
    <div class="table-responsive">
        <table id="file_list" class="table table-sm table-hover" >
            <thead>
                <th width="10%">Document added on</th>
                <th width="50%">Document Name</th>
                <th>Size</th>
                <th>Action</th>
                <th>Mark as Read</th>
            </thead>
            <tbody >
                @if ((count($Files) > 0 && $Files != null) )
                    {{-- @dd($Files) --}}
                    @foreach ($Files as $item)
                            @if ($item->is_read == 0 && $item->need_confirm == 1 )
                                <tr class="cyan lighten-5">
                            @else 
                                <tr>
                            @endif
                            <th style="font-size: 14px;" >{{ Carbon\Carbon::parse($item->created_at)->format('Y-m-d') }}</th>

                            @if($item->file_type == 'image') 
                                <th style="font-size: 14px;"  data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}" data-type="jpg" class="context-menu-files text-dark pointer">
                                    <a href="{{$item->src}}"   target="_blank" > <i class="far fa-image fa-lg text-primary  "></i> {{$item->title}} </a>
                                </th>
                            @elseif($item->file_type == 'pdf') 
                                <th style="font-size: 14px;" data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}"  data-type="pdf"  class="context-menu-files text-dark pointer">
                                    <span><i class="far fa-file-pdf fa-lg text-danger "></i> <a href="{{$item->src}}"  target="_blank"  data-name="{{$item->title}}"> {{$item->title}}  </a> </span>
                                </th>
                            @else 
                            <td></td>
                            @endif
                            
                            
                            <td style="font-size: 14px;" >{{$item->file_size}}</td>

                            @if($item->file_type == 'image') 
                            <td class="text-center" style="font-size: 14px;"  data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}" class="context-menu-files text-dark pointer">
                                <a href="{{$item->src}} " target="_blank" > <i class="fa fa-eye text-primary mr-3" title="Show" aria-hidden="true"></i> </a> 
                                <a href="{{$item->src}}" download="{{$item->title}}" target="_blank"> <i class="fa fa-download text-primary" title="Download" aria-hidden="true"></i>  </a>
                            </td>
                            @else 
                            <td class="text-center1" style="font-size: 14px;"  data-id="{{$item->id}}" data-name="{{$item->title}}" data-url= "{{$item->src}}" class="context-menu-files text-dark pointer">
                                <a href="{{$item->src}} " target="_blank" > <i class="fa fa-eye text-primary mr-3" title="Show" aria-hidden="true"></i> </a> 
                                <a href="{{$item->src}} " download="{{$item->title}}" target="_blank" > <i class="fa fa-download text-primary" title="Download" aria-hidden="true"></i> </a>
                            </td>
                            @endif
                            <td class="" >
                                    {{-- @dd($item->need_confirm) --}}
                                    @if ($item->need_confirm == 1 )
                                        <div class="custom-control custom-checkbox">
                                            @if ($item->is_read == 1 )
                                                <input type="checkbox" checked disabled class="mark_as_read" name="mark_as_read[]" value="{{$item->id}}" >
                                            @else 
                                                <input type="checkbox"  class="mark_as_read" name="mark_as_read[]" value="{{$item->id}}" >
                                            @endif
                                            
                                        </div>
                                    @else
                                        
                                    @endif
                               
                            </td>
                        </tr>
                    @endforeach
               
                @endif
            </tbody>
        </table>
    </div>

    <div class="form-group mt-5 pull-right">
        <a href="/" class="btn btn-secondary"> Back </a>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#file_list').DataTable({
            responsive: !0,
            processing: true,
            sort : false,
            "searching": false,
            "lengthChange": false,
            "paging": false,
            "info": false

        });

      
    });

 
</script>



   
    
    
    