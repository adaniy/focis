@extends('master.app')
@section('title' , ':: Manuals - Flight Operations ::')

@section('content')
@include('master.header')
  
  <style>
    body{
      overflow-x: hidden;
    }
</style>
  
<main class="mt-5 pt-5">
  <div class="container1">

    <div class="row">
      <div class="col-md-2">
        @include('include.sidebar')
      </div>

      <div class="col-md-10">
        <h4 class="text-center font-weight-bold mb-3"> </h4> 
          <div class="col-md-12">
            <nav aria-label="breadcrumb">
            </nav>
            <div class="row mb-4 pt-2 wow fadeIn">
              <div class="col-lg-12 col-md-12 ">
              
                <div id="subright">
                  <nav aria-label="breadcrumb" >
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item small"><a href="/Manuals">Home</a></li>
                      <li class="breadcrumb-item small active"><a class="text-secondary" href="/Manuals/{{$category->id}}">{{$category->name}}</a></li>
                    </ol>
                  </nav>
                    <div class="border1" id="subright1" > 
                      <section class="dark-grey-text text-center" >
                        {{--  @dd(phpinfo())  --}}
                        <div class="form-group" style="height: 10px;">
                          <h5 style="display: none; height: 100px;" class="FileLoader "><i
                              class="fas fa-spinner fa-lg1 fa-spin fa-fw"></i>
                            Please Wait .. </h5>
                        </div>

                        <div style="font-size: 20px !important" class="container3"></div>
                      </section>
                    </div>
                </div>
              </div>
            </div>
          
      
          </div>
      </div>

    </div>

   
</main>

 

  <script>
    var Gloudal_CategoryType = 'list'; 
    var CurrentPath = 0; 
    $(document).ready(function() {
      $('#new_parent').val(CurrentPath);
      $('.treeview').mdbTreeview();
      ShoFile();
      
    });

    function ShoFile()
    {
      $('.FileLoader').fadeIn();
      $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: '/GetFile/'+ {{$FileID}},
        type: 'post',
        success: function (data) {
          $('.container3').html(data.html);
          $('.FileLoader').fadeOut();
        },
        

      });
    }
    
</script>


@endsection