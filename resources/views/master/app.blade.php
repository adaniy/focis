<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title> @yield('title') </title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="{{ asset('MDB/css/bootstrap.min.css?v=1.0.4') }}" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="{{ asset('MDB/css/mdb.min.css?v=1.0.4') }}" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="{{ asset('MDB/css/style.min.css?v=1.0.4') }}" rel="stylesheet">
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script> --}}

  <link href="/vendor/js/lity/dist/lity.css" rel="stylesheet">
  <script src="/vendor/js/lity/vendor/jquery.js"></script>
  <script src="/vendor/js/lity/dist/lity.js"></script>
  <link rel="stylesheet" type="text/css" href="/vendor/js/dropzone/dropzone.css">
  {{--  <link rel="stylesheet" type="text/css" href="/vendor/js/upload-file/dropzone.min(1).css">  --}}
  <script src="/vendor/js/dropzone/dropzone.js"></script>
  <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  {{--  <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" />  --}}
  {{--  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>  --}}
  {{--  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>  --}}
  
  
  {{--  <script src="{{asset('js/datatables.min.js?v=2.2.4')}}"></script>  --}}
  

  <script src="/helper.js?v=1.0.4"></script>
  <script src="/fullscreen.js"></script> 
  <link href="https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.js" type="text/javascript"></script>
  <script src="https://swisnl.github.io/jQuery-contextMenu/dist/jquery.ui.position.min.js" type="text/javascript"></script>
  <script src="https://swisnl.github.io/jQuery-contextMenu/js/main.js" type="text/javascript"></script>
  
  <style>
    .alert{
      position: fixed;
      top: 15%;
      width: 30%;
      right: 1%;
      z-index: 999999;
    }

    .bg-reminger{
      background-color: #ffd1d6!important;
    }
    
    .bg-success-ligth{
      background-color: #00c85121!important;
    }

    .bg-secondary-ligth{
      background-color: #eee;
    }
    .center{
      margin-left: auto;
      margin-right: auto;
    }

    .item-selected{
      background-color: aliceblue;
    }
    tr { 
      text-align: left !important; 
    } 
    body{
      min-height: 300px;
      height: 100%;
      width: 100%;
      background-image: url('/img/bg-blur.jpg');
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center;
      background-attachment: fixed;

      {{--  background-color: #fcfcfc;  --}}
      {{--  overflow: hidden;  --}}
    }

    a{
      font-size: 14px;
    }
    
    #subleft {
      /*position: fixed;
      height: 100%;
      width: 100%;
      z-index: 1;
      top: 65px;
      overflow-x: hidden;
      */
      height: auto;
      border-right: 1px solid #d8d8d8;
      {{--  padding: 5px 10px;  --}}

    }

    .area-one {
      /* With the body as tall as the browser window
         this will be too */
      height: 100%;
    }
    .area-one h2 {
      height: 50px;
      line-height: 50px;
    }
    .content {
      /* Subtract the header size */
      height: calc(100% - 50px);
      overflow: auto;
    }
    

    .breadcrumb {
      padding: 5px 15px !important;
    }

    #subright {
      {{-- overflow-y: hidden; --}}
      height: 100%;
      width: 100%;
      padding-right: 1%;
      {{--  padding-left: 10%;  --}}
    }
    #subrig1ht1 {
      overflow-y: scroll;
      height: 100%;
      width: 100%;
    }
    .modal-dialog .md-tabs {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      margin: -1.5px 1rem 0 1rem;
      padding: 10px;
      -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
      box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
    }

    .center{
      margin-left: auto;
      margin-right: auto;
    }

    .context-menu-one{
     cursor: pointer; 
     color: rgb(0, 123, 255); !important;
    }

    .pointer{
      cursor: pointer; 
    }

    .text-blue {
      color: rgb(0, 123, 255); !important;
    }

    .breadcrumb-item {
      color: rgb(0, 123, 255); !important;
    }

    .breadcrumb-item .active{
      color: #6c757d;
    }  
    
      .center{
        margin-left: auto;
        margin-right: auto;
      }
  
      .item-selected{
        background-color: aliceblue;
      }

      .table.table-sm td{
        padding-top: .3rem;
        padding-bottom: .3rem;
      }
      .pointer{
        cursor: pointer;
      }

      .preloader {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-image: url('/img/material-loader0.gif');
        background-repeat: no-repeat; 
        background-color: #FFF;
        background-position: center;
     }
     @media only screen and (max-width: 600px)  {
      .dropdown-menu-right{
        right: auto;
      }
     }
    
  
  </style>
</head>
  
<body>

  <div class="modal fade" id="ResetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content modal-fluid">
          <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-key mr-1"></i> Reset Password </span> </div>
          <div class="card-body">
            <div class="row">
                <!-- Second column -->
                <div class="col-lg-12 mb-4">
                      <form id="ResetPasswordForm">
                        <!-- First row -->
                        @csrf
                        <input type="hidden" name="pass_user_id" id="pass_user_id">
                        <span class="my-4 pb-4"> Are you sure you want to generate new password to <br><strong id="pass_username"></strong> Account </span>
                            {{--  <div class="md-form mb-0">
                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" id="new_password" name="new_password" required autocomplete="new-password">
                                    
                              <label for="edit_code" data-error="wrong" data-success="right"  class="active"> New Passord</label>
                            </div>
                            <div class="md-form mb-0">
                              <input id="password-confirm" type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" required autocomplete="new-password">
                              <label for="edit_code" data-error="wrong" data-success="right"  class="active"> Confirm Password</label>
                            </div>
                              --}}
                              <br>
                          <span id="pass_user_message"></span>
                        <!-- Fourth row -->
                        <div class="row">
                          <div class="col-md-12 text-center my-4">
                              <input type="submit" value="Generate Password" class="btn btn-info btn-rounded">
                            <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4" data-dismiss="modal">Close</button>  
                          </div>
                        </div>
                        <!-- Fourth row -->
                      
      
                      </form>
                </div>
                <!-- Second column -->
      
              </div>
          </div>
        </div>
    
    </div>
  </div>

  <div class="modal fade" id="ResetPassword2Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content modal-fluid">
          <div class=" md-tabs tabs-2 light-blue darken-3">  <span class="text-white"> <i class="fas fa-key mr-1"></i> Reset Password </span> </div>
          <div class="card-body">
            <div class="row">
                <!-- Second column -->
                <div class="col-lg-12 mb-4">
                      <form id="ResetPasswordForm2">
                        <!-- First row -->
                        @csrf
                        <input type="hidden" name="pass2_user_id" id="pass2_user_id">
                        {{--  <span class="my-4 pb-4"> Are you sure you want to generate new password to <br><strong id="pass_username"></strong> Account </span>  --}}
                            <div class="md-form mb-0">
                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" id="new2_password" name="new2_password" required autocomplete="new-password">
                                    
                              <label for="edit_code" data-error="wrong" data-success="right"  class="active"> New Passord</label>
                            </div>
                            <div class="md-form mb-0">
                              <input id="password-confirm" type="password" class="form-control" id="new2_password_confirmation" name="new2_password_confirmation" required autocomplete="new-password">
                              <label for="edit_code" data-error="wrong" data-success="right"  class="active"> Confirm Password</label>
                            </div>
                            
                              <br>
                          <span id="pass2_user_message"></span>
                        <!-- Fourth row -->
                        <div class="row">
                          <div class="col-md-12 text-center my-4">
                              <input type="submit" value="Reset Password" class="btn btn-info btn-rounded">
                            <button type="button" class="btn btn-secondary btn-rounded btn-block1 my-4" data-dismiss="modal">Close</button>  
                          </div>
                        </div>
                        <!-- Fourth row -->
                      
      
                      </form>
                </div>
                <!-- Second column -->
      
              </div>
          </div>
        </div>
    
    </div>
  </div>

  <!--Modal: modalPush-->
  <div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info" role="document">
      <!--Content-->
      <div class="modal-content text-center">
        <!--Header-->
        <div class="modal-header d-flex justify-content-center">
          <p class="heading">Reading Confirmation</p>
        </div>

        <!--Body-->
        <div class="modal-body">
          <input type="hidden" name="test1" id="test1">

          <i class="fas fa-check fa-4x animated rotateIn mb-4"></i>

          <p>Confirmed an accepting reading this file</p>

        </div>

        <!--Footer-->
        <div class="modal-footer flex-center">
          <button id="confirm" class="btn btn-info">Yes</button>
          <a type="button" id="cancel" class="btn btn-outline-info waves-effect" data-dismiss="modal">No</a>
        </div>
      </div>
      <!--/.Content-->
    </div>
  </div>
<!--Modal: modalPush-->

<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  data-keyboard="false" data-backdrop="static"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class=" md-tabs tabs-2 bg-danger darken-3">  <span class="text-white"> <i class="fas fa-folder-open mr-1"></i> Delete </span> </div>
    <form action="" method="POST" id="DeleteForm" >
      <input type="hidden" name="deleteType" id="deleteType">
      @csrf
     
      <div class="modal-body mb-3">
        <div class="md-form form-sm">
          <span> Are you sure you want to delete </span>  <strong id="deleteName"></strong>
          <input type="hidden" name="deleteID" id="deleteID" >
          <strong class="text-danger" style="display: none" id="ErrorMessage">Sorry, this folder is not empty!!</strong>
        </div>

          <button class="btn btn-danger btn-sm btn-block1 my-4" type="submit">Delete </button>
          <button type="button" class="btn btn-secondary btn-sm btn-block1 my-4" data-dismiss="modal">Close</button>  

      </div>

    </form>
  
  </div>
</div>
</div>


  <div class="preloader">
  </div>
  <div class="container" style="max-width : 95% !important">
    <div class="alert alert-success alert-dismissible fade show1 z-depth-2"  width="30%" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong class="message"></strong> 
    </div>

    <div class="alert alert-danger alert-dismissible fade"  width="30%" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong class="message"></strong> 
    </div>
    @yield('content')
  </div>
  @include('master.footer')
</body>

</html>

<script>
  $(window).load(function() {
    $('.preloader').fadeOut('slow');
  });
</script>
