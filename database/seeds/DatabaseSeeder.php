<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'code' => 'adn',
            'name' => 'admin',
            'email' => 'admin@chamwings.com',
            'password' => Hash::make('we123456'),
            'role_id' => 1, // Admin
            'rank_id' => 1, // Admin
            'is_active' => 1, 
            
        ]);

        DB::table('users')->insert([
            'code' => 'use',
            'name' => 'user',
            'email' => 'user@chamwings.com',
            'password' => Hash::make('we123456'),
            'role_id' => 2, //User
            'rank_id' => 2, 
            'is_active' => 1, 
        ]);
    }
}
