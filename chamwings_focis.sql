-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 05, 2020 at 06:57 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chamwings_focis`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `view_at_sidebar` int(11) NOT NULL,
  `order_at_sidebar` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `view_at_sidebar`, `order_at_sidebar`, `created_at`, `updated_at`) VALUES
(19, 'Aircraft', 0, 1, 2, '2020-04-29 02:22:15', '2020-04-29 02:22:15'),
(15, 'Flight Operations Manuals', 0, 0, 0, '2020-04-02 03:12:48', '2020-04-02 03:12:48'),
(10, 'Airbus Manuals', 0, 0, 0, '2020-03-10 17:29:43', '2020-03-10 17:29:43'),
(11, 'FOCIS', 0, 1, 1, '2020-03-17 09:23:30', '2020-03-17 09:23:30'),
(12, 'RED', 11, 1, 1, '2020-03-17 09:23:45', '2020-03-17 09:23:45'),
(13, 'YELLOW', 11, 1, 2, '2020-03-17 09:23:57', '2020-03-17 09:23:57'),
(14, 'BLUE', 11, 1, 3, '2020-03-17 09:24:11', '2020-03-17 09:24:11'),
(20, 'YK-BAA', 19, 1, 1, '2020-04-29 02:22:34', '2020-04-29 02:22:34'),
(21, 'YK-BAB', 19, 1, 2, '2020-04-29 02:22:46', '2020-04-29 02:22:46'),
(22, 'YK-BAE', 19, 1, 3, '2020-04-29 02:22:59', '2020-04-29 02:22:59'),
(23, 'YK-BAG', 19, 1, 4, '2020-04-29 02:23:09', '2020-04-29 02:23:09'),
(24, 'Archive', 11, 0, 0, '2020-04-29 02:24:11', '2020-04-29 02:24:11'),
(25, 'SCCM Flight Reports', 0, 0, 0, '2020-05-03 05:56:14', '2020-05-03 05:56:14'),
(0, 'root', 0, 0, 0, '2020-07-01 21:00:00', '2020-07-01 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `src` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `need_confirm` int(11) NOT NULL DEFAULT '1',
  `is_downloaded` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `category_id`, `name`, `title`, `file_type`, `file_size`, `src`, `need_confirm`, `is_downloaded`, `created_at`, `updated_at`) VALUES
(22, 13, '1584455539.pdf', '3 FOCIS  OPS-C-03Y Signed.pdf', 'pdf', '1.71 MB', '/files/1584455539.pdf', 1, 1, '2020-03-17 09:32:19', '2020-07-05 09:57:37'),
(23, 13, '1584455784.pdf', '4 Operational Flight Plan  OPS-C-04Y.pdf', 'pdf', '329.75 KB', '/files/1584455784.pdf', 0, 1, '2020-03-17 09:36:24', '2020-03-17 09:36:24'),
(24, 13, '1584456077.pdf', '7 Stand by SBY and AV available  OPS-C-07Y.pdf', 'pdf', '1.58 MB', '/files/1584456077.pdf', 1, 1, '2020-03-17 09:41:17', '2020-03-17 09:41:17'),
(25, 13, '1584456476.pdf', '6 Communication with Rostering Officers   OPS-C-06Yy.pdf', 'pdf', '1.78 MB', '/files/1584456476.pdf', 1, 1, '2020-03-17 09:47:56', '2020-03-17 09:47:56'),
(49, 13, '1585763772.pdf', '5 Coronavirus Alert  OPS-C-05Y Rev 1.pdf', 'pdf', '4.06 MB', '/files/1585763772.pdf', 1, 1, '2020-04-01 11:56:12', '2020-04-01 11:56:12'),
(50, 13, '1585764865.pdf', '9 Crew Personal Documents Check  OPS-C-09Y.pdf', 'pdf', '1.94 MB', '/files/1585764865.pdf', 1, 1, '2020-04-01 12:14:25', '2020-04-01 12:14:25'),
(51, 13, '1585764905.pdf', '10 Commitment to Duty Time Limitations  OPS-C-10Y.pdf', 'pdf', '1.54 MB', '/files/1585764905.pdf', 1, 1, '2020-04-01 12:15:05', '2020-07-01 07:09:21'),
(52, 13, '1585764969.pdf', '11 Onboard Survey OPS-C-11Y.pdfcc', 'pdf', '1.69 MB', '/files/1585764969.pdf', 1, 1, '2020-04-01 12:16:09', '2020-07-05 14:44:52'),
(55, 13, '1585765024.pdf', '14 Coronavirus Alert New Instructions from KRT airport  OPS-C-14Y.pdf', 'pdf', '1.34 MB', '/files/1585765024.pdf', 1, 1, '2020-04-01 12:17:04', '2020-04-01 12:17:04'),
(56, 13, '1585765270.pdf', '16 Announcement for Public Health Passenger Locator Form OPS-C-16Y.pdf', 'pdf', '1.7 MB', '/files/1585765270.pdf', 0, 1, '2020-04-01 12:21:10', '2020-07-01 07:06:49'),
(57, 13, '1585765294.pdf', '17 Suspension of on boa', 'pdf', '1.41 MB', '/files/1585765294.pdf', 0, 1, '2020-04-01 12:21:34', '2020-07-01 07:31:59'),
(58, 13, '1585765385.pdf', '19 New Form for Yerevan Airport  OPS-C-19Y.pdf', 'pdf', '1.42 MB', '/files/1585765385.pdf', 1, 1, '2020-04-01 12:23:05', '2020-04-01 12:23:05'),
(69, 13, '59_1585816760.pdf', '12 Coronavirus Alert Public Health Passenger Locator Form OPS-C-12Y.pdf', 'pdf', '131.44 KB', '/files/59_1585816760.pdf', 1, 1, '2020-04-02 02:39:20', '2020-07-01 07:10:59'),
(71, 13, '71_1585818352.pdf', '18 Additional instructions reference Checking passenger body temperature  OPS-C-18Y.pdf', 'pdf', '356.72 KB', '/files/71_1585818352.pdf', 1, 1, '2020-04-02 03:05:52', '2020-04-02 03:05:52'),
(72, 13, '72_1585818467.pdf', '8 Commander Discretion Report  OPS-C-08Y.pdf', 'pdf', '1.57 MB', '/files/72_1585818467.pdf', 1, 1, '2020-04-02 03:07:47', '2020-04-02 03:07:47'),
(73, 15, '73_1585819144.pdf', 'OM-A Rev11 JAN 2020.pdf', 'pdf', '30.25 MB', '/files/73_1585819144.pdf', 1, 1, '2020-04-02 03:19:05', '2020-04-02 03:19:05'),
(74, 15, '74_1585820361.pdf', 'OM-B Rev 01 2017.pdf', 'pdf', '2.09 MB', '/files/74_1585820361.pdf', 1, 1, '2020-04-02 03:39:21', '2020-04-02 03:39:21'),
(75, 15, '75_1585820362.pdf', 'OM-C Rev 01 Aug 2017.pdf', 'pdf', '1.2 MB', '/files/75_1585820362.pdf', 1, 1, '2020-04-02 03:39:22', '2020-04-02 03:39:22'),
(76, 15, '76_1585820363.pdf', 'OM-D Rev 02 JULY 2017.pdf', 'pdf', '10.24 MB', '/files/76_1585820363.pdf', 1, 1, '2020-04-02 03:39:23', '2020-04-02 03:39:23'),
(90, 13, '79_1587648471.pdf', '21 Coronavirus Additional Mandatory Measures OPS-C-21Y.pdf', 'pdf', '228.96 KB', '/files/79_1587648471.pdf', 1, 1, '2020-04-23 07:27:51', '2020-07-01 07:11:03'),
(92, 0, '91_1591702313.pdf', 'OM-A _Old Structure.pdf', 'pdf', '154.34 KB', '/files/91_1591702313.pdf', 1, 1, '2020-06-09 05:31:53', '2020-06-09 05:31:53'),
(93, 0, '93_1591702611.pdf', 'COVID19-sympto', 'pdf', '95.14 KB', '/files/93_1591702611.pdf', 1, 1, '2020-06-09 05:36:51', '2020-07-05 13:31:54'),
(94, 14, '94_1593076083.pdf', 'PO-2019 (1).pdf', 'pdf', '91.51 KB', '/files/94_1593076083.pdf', 0, 1, '2020-06-24 21:00:00', '2020-07-01 07:39:46'),
(95, 24, '95_1593681394.pdf', 'PO-2019.pdf', 'pdf', '91.51 KB', '/files/95_1593681394.pdf', 1, 1, '2020-07-02 06:16:34', '2020-07-02 06:16:34'),
(96, 0, '96_1593681437.pdf', 'PO-2019.pdf', 'pdf', '91.51 KB', '/files/96_1593681437.pdf', 1, 1, '2020-07-02 06:17:17', '2020-07-02 06:17:17'),
(97, 24, '97_1593681549.jpg', '1 (5).jpg', 'image', '50.94 KB', '/files/97_1593681549.jpg', 1, 1, '2020-07-02 06:19:09', '2020-07-02 08:57:08'),
(99, 24, '98_1593690560.pdf', 'Request1', 'pdf', '105.2 KB', '/files/98_1593690560.pdf', 1, 1, '2020-07-02 08:49:20', '2020-07-02 09:18:44');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(16, 'Group 2', '2020-06-18 06:54:36', '2020-06-22 09:44:07'),
(18, 'Group 1', '2020-06-22 07:02:18', '2020-06-22 09:43:57'),
(19, 'Group 3', '2020-06-22 09:50:38', '2020-06-22 09:50:38'),
(21, 'Group 5', '2020-06-22 09:50:48', '2020-06-22 09:50:48'),
(22, 'Group 4', '2020-06-29 03:19:59', '2020-06-29 03:19:59');

-- --------------------------------------------------------

--
-- Table structure for table `group_file`
--

DROP TABLE IF EXISTS `group_file`;
CREATE TABLE IF NOT EXISTS `group_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `file_id` (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_file`
--

INSERT INTO `group_file` (`id`, `file_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 99, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(2, 99, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(3, 99, 19, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(4, 99, 22, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(5, 99, 21, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(6, 97, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(7, 97, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(8, 97, 22, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(9, 97, 21, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(10, 96, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(11, 96, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(12, 96, 22, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(13, 96, 21, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(14, 94, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(15, 93, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(16, 93, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(17, 93, 19, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(18, 92, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(19, 92, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(20, 92, 19, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(21, 90, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(22, 90, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(23, 90, 19, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(24, 69, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(25, 69, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(26, 69, 21, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(27, 55, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(28, 55, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(29, 52, 18, '2020-07-05 15:14:21', '2020-07-05 15:14:21'),
(30, 52, 16, '2020-07-05 15:14:21', '2020-07-05 15:14:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(179, '2014_10_12_000000_create_users_table', 2),
(180, '2014_10_12_100000_create_password_resets_table', 2),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(181, '2020_02_04_110659_create_categories_table', 2),
(182, '2020_02_04_110718_create_files_table', 2),
(183, '2020_02_04_110746_create_user_files_table', 2),
(184, '2020_02_05_094602_create_roles_table', 2),
(185, '2020_02_27_094242_create_ranks_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ranks`
--

DROP TABLE IF EXISTS `ranks`;
CREATE TABLE IF NOT EXISTS `ranks` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ranks`
--

INSERT INTO `ranks` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'COO', NULL, NULL),
(2, 'Flight Operations Director', NULL, NULL),
(3, 'TRI Pilot', NULL, NULL),
(4, 'Cabin Crew manager', NULL, NULL),
(5, 'Administrative Consultant', NULL, NULL),
(8, 'First Officer', NULL, NULL),
(9, 'Rostering Officer', NULL, NULL),
(10, 'Grooming And Service Manager', NULL, NULL),
(11, 'Flight Operations Control Manager', NULL, NULL),
(12, 'DISPATCHER', NULL, NULL),
(13, 'Trainee Dispatcher', NULL, NULL),
(14, 'Catering Assistant', NULL, NULL),
(15, 'Crew Controller', NULL, NULL),
(16, 'Emission Manager', NULL, NULL),
(17, 'Flight Operation Support Officer', NULL, NULL),
(18, 'Route Planning Manager', NULL, NULL),
(19, 'Cadet', NULL, NULL),
(20, 'Senior Cabin Crew Member', NULL, NULL),
(21, 'Cabin Crew Member', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'User', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sidebar_category`
--

DROP TABLE IF EXISTS `sidebar_category`;
CREATE TABLE IF NOT EXISTS `sidebar_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rank_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_authorized` int(11) NOT NULL DEFAULT '0',
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `code`, `email_verified_at`, `password`, `rank_id`, `role_id`, `is_active`, `is_authorized`, `note`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@chamwings.com', 'adn', NULL, '$2y$10$BUkmnqQSZyiRsIY2Hn.q..WgC4W5IN11WtsnPNiQshTUXXwsS8mrC', 22, 1, 1, 1, NULL, NULL, NULL, '2020-04-13 08:05:09'),
(3, 'MOUSSA BOUTROS', 'coo@chamwings.com', 'MBT', NULL, '$2y$10$Z/a.3Vi2t/Xpj3PP5sfQCO7UWbwIyCII8X6g6Nyp22D7Kobpk2dTK', 1, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(4, 'Stelios Galides', 'Sgalides@chamwings.com', 'SGA', NULL, '$2y$10$vdWX0Oymszl0WXBOwHdkEOPPzE3fFHSwyDm9wcj27yT9fnBPCpfAu', 2, 1, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(5, 'MUHAMMAD FARNAWANY', 'mfarnawany@chamwings.com', 'MFN', NULL, '$2y$10$9OCppDp1.IaDQxG4HMCc2uO9f4kvlbtfmI5n3rhsxut/wO71AYGVK', 3, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(6, 'MOHAMED ABAZA', 'mabaza@chamwings.com', 'MBZ', NULL, '$2y$10$F98blhQVJYHurami6OzTHuRiD.dVL7S/Etdu3xjlv3dvzb94cXIu6', 3, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(7, 'Rana Daghlawi', 'rdeghlawi@chamwings.com', 'RDI', NULL, '$2y$10$RXk5YgW1/6xIeQZuS7t4D.QUJFXreXj8seXMIAoDkFhY7IrAp4E6W', 4, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(8, 'Ali Omran', 'Aomran@chamwings.com', 'AOM', NULL, '$2y$10$o9j0xI2/saftDq014mSCJ.ZMCkMQMQ03U8QuS/iWu5DZ.oI2GxFMu', 5, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(9, 'Sham Aloush', 'Saloush@Chamwings.com', 'SAL', NULL, '$2y$10$w5gFjqYJcrRbgIwrPfcVGO4mjsxJqvYKL2Oi1HH/j845278FvpOyO', 6, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(10, 'Marah Aboud', 'Maaboud@chamwings.com', 'MAS', NULL, '$2y$10$8vKYhn1.l6bjDMZOEb2VCuEn7gLzaL0eL2HZz5uOWmHVRajWXLg7m', 7, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(11, 'Walaa Tallaj', 'wtallaj@chamwings.com', 'WTA', NULL, '$2y$10$BUkmnqQSZyiRsIY2Hn.q..WgC4W5IN11WtsnPNiQshTUXXwsS8mrC', 9, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(12, 'Cathrine Shuqair', 'cshukair@chamwings.com', 'CSH', NULL, '$2y$10$loI4KgGAtmnIyw9DT2WyZe5Z7P6cIWsrExrnettYAxeyTKDDDZtZS', 9, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(13, 'Hind Mreisheh', 'Hmraishah@chamwings.com', 'HMR', NULL, '$2y$10$4M0Zi6i3A9HtYGV18xKdwetngMkh9Xh5f1PzLuml.KkIOf6gNeHUq', 10, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:06'),
(14, 'Walid Hanan', 'whanan@chamwings.com', 'WHA', NULL, '$2y$10$TRl86m0SKkTyzvM0ABlUZOthlKdFmAW.BajGeY6oIot2Q6CMiZxZK', 11, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(15, 'Yaman Shammout', 'Yshammout@chamwings.com', 'YSH', NULL, '$2y$10$MFOYrodgvF3J6.Ml/KuNy.CBRi/IXA76KJDqlLksyfPi9SI5HYhTe', 12, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(16, 'Yamen Martini', 'Ymartini@chamwings.com', 'YMR', NULL, '$2y$10$TGzZxf4llYxaBjufUFpOtujOV0ef6a8PJqLybo52gfPKSpJnbcY9O', 12, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(17, 'Karim Dwair', 'kdwair@chamwings.com', 'KAR', NULL, '$2y$10$sRPrS2Aa0iyeFufyYp3cc.FNMyW40rKmzy8X.U6S01MUWhxG29bIO', 13, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(18, 'Khalil Haji Othman', 'Kothman@chamwings.com', 'KHO', NULL, '$2y$10$9U7SU/DVudAXdDgx9JHstOa1mk.aIVnv3oJnCeQAsKbllZIyL6lza', 13, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(19, 'Khaled Al-Tajjar', 'ktajjar@chamwings.com', 'KHT', NULL, '$2y$10$jTtukjMxggWvKY5S99PLG.kPfMV51uk4mv28lRDyl1/aQ2T7LTnzG', 13, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(20, 'Yamen Al-Saaie', 'Ysaaie@chamwings.com', 'YSA', NULL, '$2y$10$Ua1Bb8TJrhgSzASAvQJmZ.gWIV/Vn4QUD7o49LR.Z7o3SN.eY7xku', 13, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(21, 'Danial Al-Dakkak', 'Daldakkak@chamwings.com', 'DAD', NULL, '$2y$10$.znH9/aHZxCH966nzmzfVOcsqndtnBz5cUAkc0wggRy/O1VP9lbZ6', 13, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(22, 'Ghadir Asaad', 'Gasaad@chamwings.com', 'GAS', NULL, '$2y$10$RyMCCtn8xLjwSs.QVGkHJuH7RNmh9aPTUZCofGesaYcZxQyyCYH1u', 13, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:07'),
(23, 'Yaman Asaad', 'Yasaad@Chamwings.com', 'YAS', NULL, '$2y$10$XhShFWX843YL.3cYfP1bg.foedBTEv8LYUWWDIMGCvs50M1Sp7FAi', 14, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:10'),
(24, 'Georgeet Asi', 'Goasi@chamwings.com', 'GEA', NULL, '$2y$10$0plm5fNGRA/H3iTYf54eNOuPKq9uO4cX.OHhtiOfXz6Usau1L.nRG', 15, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:10'),
(25, 'Talal Asaad', 'Tasaad@chamwings.com', 'TAS', NULL, '$2y$10$dO2FJ7JtN2Bd7SQS9SboO.qYFNdExwB5PBP14hpp9.QoXxydBxCwS', 16, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(26, 'Hasan Qutainy', 'Hkoutaini@chamwings.com', 'HKO', NULL, '$2y$10$Vnsm0l.iRguu/u0v8JimweLHnMtUxHx9RhHD9uh3wVoYPkDmtXCmm', 17, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(27, 'Abdullnasser Al-Afandi', 'a.alafandi@chamwings.com', 'ANA', NULL, '$2y$10$nCaBJBaLPwNTDVCf5Z6k.emGD7CuHH4P54i7dYbgTh95o.QxtsM0S', 18, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(28, 'Michel AL-Hosh', 'Malhosh@chamwings.com', 'MIH', NULL, '$2y$10$AJNbxFnqPyoqe/v8m8Os7ehS4xICZBzTIOIaYwxa9qlBUEGwZDMve', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(29, 'Mohammad AL-Nasani', 'Malnasani@chamwings.com', 'MNS', NULL, '$2y$10$3oGYV.DCD7HpLByylU8unOI.DdWWXM4P0sBbi5yZ4NsgAWzrtTT9.', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(30, 'Badia AL-Diratani', 'Baldiratani@chamwings.com', 'BDI', NULL, '$2y$10$NvTDYzn2317u8mF.lXj.pONN4oWWExQyHErBYGEPuohfX9S52LySS', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(31, 'Ali AL-Lahham', 'Allahham@chamwings.com', 'ALA', NULL, '$2y$10$d/CqAiFNrXPUmBe4aKRGquUsMWEOfg5frdMKDmdXNNfRzDqyURZSa', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(32, 'Lous Boutros', 'lboutros@chamwings.com', 'LBO', NULL, '$2y$10$M112BXc43TMKK/abo94qiekvshturS9G19Auc3tdFpkYHLYJ6fUP6', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(33, 'Ahmad Chammout', 'ahshammout@chamwings.com', 'SHA', NULL, '$2y$10$I2ecMzsVvtZASC2fdi87S.nJqPFzjPMFHsWX/HHK4g5xdqrcE/FSK', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(34, 'AL-Hakam Fahed', 'hfahed@chamwings.com', 'HFD', NULL, '$2y$10$1LvqBFucVKRrb1ElSIQZ0uSAOXi5FfiYE2LFAnbIi5WzlOXcGZWPq', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:11'),
(35, 'Modar Jabbour', 'Mjabbour@chamwings.com', 'MJR', NULL, '$2y$10$cVeBHUdG2X1.HO5J.1hzAedEkQqayVnQwfHflk9O4OSVj2HyRg79q', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:12'),
(36, 'Jawad Kassam', 'jkassam@chamwings.com', 'JKA', NULL, '$2y$10$2pG24IabC8aR/7vz6kJc/uDNxdZbaEV2igoQmm2uIhGJKpL5mwHFy', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:12'),
(37, 'Christian Lotfi', 'cloutf@chamwings.com', 'CHL', NULL, '$2y$10$cdyOh4fIE.8AAjD/J80m5.bqfDL.WSP1eFFpV.BNRJRI62WpL8P6a', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:12'),
(38, 'Aadel Sarra', 'asarra@chamwings.com', 'ASA', NULL, '$2y$10$cENi4AnzfDV5vY/FEd12rupEoXzl4FaBdI72nGIjAUJegVE4NGEEi', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:12'),
(39, 'Abdulrahman AL-Ibrahim', 'Aibrahim@chamwings.com', 'ABI', NULL, '$2y$10$TRDAxwj4.w5M6xzfR0bQ0O63m0.3eejDD/f6e0i8IstAVyUO6w6wa', 8, 2, 0, 1, NULL, NULL, NULL, '2020-06-24 09:02:03'),
(40, 'Qutaiba Ayoub', 'Qayoub@chamwings.com', 'QAB', NULL, '$2y$10$U9lc11tAyt1zI5Gq1f8BFeIuD.sMZ3w/pNQCwTdZ1HAq3GjwgzFJe', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:12'),
(41, 'Jamal Farzan', 'Jfrzan@chamwings.com', 'JAF', NULL, '$2y$10$rBkcmM217I/YK1/GwXkFfeCxqRqYP9NDp588f2/q99QsF26lP7SIG', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:12'),
(42, 'Bakri Babouli', 'Babouli@chamwings.com', 'BBA', NULL, '$2y$10$99t/77jJM2VvTcolJaMo/u5KDdXpdgx9NR53IEsEyOqoDtbAlKt3m', 8, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:12'),
(43, 'Mhawish AL-Hammad', 'Mhammad@chamwings.com', 'MWH', NULL, '$2y$10$jOUeX4Zu/4YuWk5ZFAHNuOLvL.W9SMWA9q7Z0gfYprBNAYwl0CoG6', 19, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:15'),
(44, 'Kaled AL-Edwani', 'Kaladwani@chamwings.com', 'KNI', NULL, '$2y$10$5x2ep.Luyq2KcHwVLWiwtuKfQ34zeqo3BuAh.d4kcSWilJH02zXou', 19, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:15'),
(45, 'Tala  AL-Edwani', 'Taladwni@chamwings.com', 'TNI', NULL, '$2y$10$NYgB61L78Ke/cMh/J9l95.yzTeyZRAIGL0Efpl.L50bPN.cGRsqfK', 19, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:15'),
(46, 'Yanal Badwan', 'Ybadwan@chamwings.com', 'YIB', NULL, '$2y$10$8vU49ZkrQkOzGJ78XT5ztOzR9wACiuVn2GkUnlpwm4TumnbGKvsCG', 19, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(47, 'Abdul Aziz AL-talab ', 'altalab@chamwings.com', 'ATB', NULL, '$2y$10$8fPoAd8UWppKHlqplEu3iem.X4RxdJs/1AkCeYfFgD39uMyb.3o76', 19, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(48, 'Ehab Al-Dakkak', 'edakkak@chamwings.com', 'EDK', NULL, '$2y$10$bTEeLW1KAjRj2g9sIPmlWOFwwq7.IDNcRfrYGYat2XYLM.aB1uW3G', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(49, 'Anas Faour', 'afaour@chamwings.com', 'AFR', NULL, '$2y$10$3JDnPtkkRqTnli4fLtRvH..MKl71iubuSAgUct4.F11KPW6SPbkbW', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(50, 'Ahmad Hussam Al-Halabi', 'Ahalabi@chamwings.com', 'AHI', NULL, '$2y$10$Kbm5muEtQdVcox9PVUj0Ge5YezGqDUe259p0Tbo35XJWJS0VfsKRK', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(51, 'Mouhamad Ibraheem', 'mibrahim@chamwings.com', 'MIM', NULL, '$2y$10$fnam2sVnnSK238q9R7cch.ZqcXEPPA/aTuih.vlDeQ.cZncQeNcPy', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(52, 'Gihan Abdullah', 'Gabdalla@chamwings.com', 'GAA', NULL, '$2y$10$AxRt0tWcV63u5492xXHSuep9FUECtesacOLfyhDnEGKz//2pIlM6G', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(53, 'Ruwaida Abu-hamdan', 'rhamdan@chamwings.com', 'RAH', NULL, '$2y$10$2ZQS5vW7EVTmGHXCK8gg..9CYkxAF5XGwRILig6BAM9xBxCRTlUVi', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(54, 'Issam Rofael', 'Iroufael@chamwings.com', 'IRO', NULL, '$2y$10$vTLKsbMkmzvEd/gy0QkdjOF5s2lKyFXMQ4S3LexaZduzQZlK3hFI2', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(55, 'Mouhamad Jad-allah Dahman', 'Mdahman@chamwings.com', 'MDN', NULL, '$2y$10$RO8YhBcAExh4k9x/1QwUnOQkZmMv8WPhkkMgDZ4k6q4CDRzP21REi', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:16'),
(56, 'Walaa Haski', 'Whaski@chamwings.com', 'WHI', NULL, '$2y$10$Z6NxXWpYXK21Cb/kBbyJseMtCmsSMnDP6hkPwNmJNQuTnu7ddyqzm', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:17'),
(57, 'Amro Al-Afandi', 'Afandi@chamwings.com', 'AFI', NULL, '$2y$10$4SF5qTuGdSmsjVZkpUBF4ed/T4wziRQBYsFw/oNYTA9PrbpxSFdum', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:17'),
(58, 'Darine Arkawi', 'Darkawi@chamwings.com', 'DAI', NULL, '$2y$10$IfQBpR2rIUJziGFe8GqlA.NBNcY9IV6azeoUYb17hmIBv6ohYLSSC', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:17'),
(59, 'Mouhamad Ayman Dyarbekrli', 'abakerly@chamwings.com', 'ADI', NULL, '$2y$10$l0mwimjeWN1nP8X.dru.ged7R22rr5x2bSMSfIcK9lZqlNCNjeo82', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:17'),
(60, 'Suzana Al-Moaz', 'suzana@chamwings.com', 'SMZ', NULL, '$2y$10$jJ3av4OG1fic.dI4BUNSwuQJyoK2wsHaqs.77knCpVz5nvgDopCYe', 20, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:17'),
(61, 'Leen Abdo Othman', 'Lothman@chamwings.com', 'LAN', NULL, '$2y$10$xjLi.1ige40Aq9TQxpUJXevbOVmZJaufVsdD4VtOHnfz6e/Vf84Jy', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:17'),
(62, 'Rahaf Badran', 'Rbadran@chamwings.com', 'RAB', NULL, '$2y$10$JOK.NqTRCrrFJIXlCh7VT.gWRqieiA8urbozZjYW.ldq1qatvMMQ.', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:17'),
(63, 'Mouhamad Wael Hadad', 'whadad@chamwings.com', 'WHD', NULL, '$2y$10$twvFY9hVTmElFgC.W.nwi./Q2jJ68h19BBc2KdkV.OSLqooyINya6', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:20'),
(64, 'Grace Ibraheem', 'gibrahim@chamwings.com', 'GIM', NULL, '$2y$10$k6H.oxXad0g4e7PvJKk4QOA2L7ccHbmXGb81geKxxSsrqRL9ula0m', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:20'),
(65, 'Namaa Youssef', 'Nyoussef@chamwings.com', 'NYF', NULL, '$2y$10$QLi.tqfos4VASZ465EXMSuewBUU5NQ0dBUnJ01JX1ip254Q9xxmNy', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:20'),
(66, 'Haya Laila', 'hlaila@chamwings.com', 'HLA', NULL, '$2y$10$5bYniqf2hkOz7b7se7HSvOasMuoHrj1CuYN4ycMAEOpp4iHWgv8Iu', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:20'),
(67, 'Dina Serieh', 'Dserieh@chamwings.com', 'DSH', NULL, '$2y$10$kLtclTlhZ/BYowO3zMlGE.KivwT90d5eDl7euRZqYMCitcZXP/4Yi', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(68, 'Hayan Al-Fakhani', 'Hfakhane@chamwings.com', 'HFI', NULL, '$2y$10$3lvbdPBT.wWZ2ngOwkP2beb7sldmwF9hklT1XdD4wD0OrZvn7lCVm', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(69, 'Sarah Al-Frayeh', 'Sfrayeh@chamwings.com', 'SFH', NULL, '$2y$10$SjrLIqOgRC4osU1TDDzmsOzKziB.gxEG6FQKIOZ9xggPgKH6G7LeW', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(70, 'Carolina Kandalaft', 'Ckandalaft@chamwings.com', 'CKT', NULL, '$2y$10$Ey7gXmaGlFrB9rZ.RM2/.efz8YVUeR.A4vnZ0rklxHE8oAIgEiN12', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(71, 'Rabei Al-Nasser', 'Ralnassr@chamwings.com', 'RAR', NULL, '$2y$10$EtiRt2xp2q2s3o89ROkiJ.wm9JQLyjZeCKkFwGUxMm0yMnDLogyMq', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(72, 'Rabea Bathoush', 'Rbathoush@chamwings.com', 'RBH', NULL, '$2y$10$JSl4uq8WNm8EFgJ0hWj6J.XAaiJDpGu88d7zqa8WvvnhtnJfi/MIm', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(73, 'Ahmad Salameh', 'Asalameh@Chamwings.com', 'ADH', NULL, '$2y$10$1NINaBcI7c.9PMcYtvwTtOarQrcL1j9F7MEWssEWrFgTDHuzbms1m', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(74, 'Bilal Daloul', 'bdallol@chamwings.com', 'BDL', NULL, '$2y$10$Ul7qiEMqXY8oKb/XyFJY5OyXPgiQEIu8YtM6keemTVMP4Rnm0clBa', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(75, 'Malakeh Al-Khouri', 'Mkhouri@chamwings.com', 'MKI', NULL, '$2y$10$6vsoIHBXZhDkJoorTpbAEuIcm3C8gmQIASn2eaVc8Ly0fhPfW9LQm', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(76, 'Nour Hassani', 'Nhassani@chamwings.com', 'NHI', NULL, '$2y$10$XuYE12HGgrTHfGjdyb/9y.IBYVXcwWBGwYQSUoU41SLD986AQEyia', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(77, 'Nourhan Safetly', 'Nsafetly@chamwings.com', 'NSY', NULL, '$2y$10$zvzURb0RzTi/PCQG72hWA.iryuRjivBXg5z4odohNxGINoAw9Pgqa', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:21'),
(78, 'Yazan Shummari', 'Yshummari@chamwings.com', 'YSI', NULL, '$2y$10$im7c.lYSwp9F2BwcJO4br.kCBCZebrtzzdjpAsTIgded1Yo2.46i.', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:22'),
(79, 'Odai Al-Kassas', 'oalqassas@chamwings.com', 'OQS', NULL, '$2y$10$1Vgf89Lo0rFvwJt9yfJh/OzRPotzsytUadDf/y6Kfk8t/ZoVBdMH2', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:22'),
(80, 'Yara Baradei', 'Ybaradei@chamwings.com', 'YBI', NULL, '$2y$10$jyqonfk7nYDfPJBUEQahAujR5PIKMd9qsf.rd7M8oWvvSJ0hVpnVi', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:22'),
(81, 'Rasha Moussa', 'Rmousa@chamwings.com', 'RMO', NULL, '$2y$10$FFFHhewTKm9xDi73Jl0O3ey6HmuSyKk3VXFzV5a8APbm8NYF8kaTC', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:22'),
(82, 'Zeina Al-Atrash', 'Zalatrash@chamwings.com', 'ZAS', NULL, '$2y$10$OthTNN3RkM/nsKo2js6QIudk4s9kmkx0FmOo6AXX/xavXA/o3Y4k2', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:22'),
(83, 'Jamil Nader', 'Jnader@chamwings.com', 'JLR', NULL, '$2y$10$yHtJSJh2wSoq3bSJ3.POBOhaylO8AgEEuElnny7VM1ElOz8uV2r7W', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:25'),
(84, 'Ranim Gharbi', 'rgharbi@chamwings.com', 'RGI', NULL, '$2y$10$KPZ/JEyyShxQHIQ6nu4d3O/OYVEighO/Pz4dhPTI0JbABG.wmdNZe', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:25'),
(85, 'Sarah Moulayes', 'smoulayes@chamwings.com', 'SMS', NULL, '$2y$10$fy9Lh5L/EvNjl1xn1HTm5uGqJKGvvRiZ6fBX3wGgjEvpjqhfKOjtS', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:25'),
(86, 'Omar Haski', 'ohaski@chamwings.com', 'OHI', NULL, '$2y$10$eDH.hKNZJutY1dDQmaGVT.gGueAz6xq7CsHvgSedLaKOSYS2kWhhm', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:25'),
(87, 'Maya Mohsen', 'Mamohsen@chamwings.com', 'MMN', NULL, '$2y$10$YRjE08AQVvN72Kbq0W9gW.irFZRUQaItp1EN0nvULGoZoFPndfAHK', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:25'),
(88, 'Abdullmasih Wasouf Saad', 'Awsaad@chamwings.com', 'AWD', NULL, '$2y$10$kdT0t2Mt2uO4TxIUWVTsReIchMsSsbrMk7hMZ8MQhxC4OtZHP1Uku', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:25'),
(89, 'Sewar Al-Halabi', 'Salhalabi@chamwings.com', 'SAI', NULL, '$2y$10$hnpqXIiX6mjnWel5lshJX.10YV.iERLpOnIgiJRY4FoKcK5Nk5OBG', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(90, 'Cathrine Nassour', 'cnasour@chamwings.com', 'CTR', NULL, '$2y$10$NOmrEVOo.m4SDn2d425Oa.XKuKaKlzYNHdlO0n6QJlt/Sf1gLOJEe', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(91, 'Nadra Kam-naqsh', 'nkmnaksh@chamwings.com', 'NAH', NULL, '$2y$10$lbvvAtnY9Rw.cRvdQ16PCuuV2/K.RWpOBkZS2N3tEwGblO8629xQ2', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(92, 'Haidara Khalil', 'Hkhalil@chamwings.com', 'HKL', NULL, '$2y$10$swTCz9JVbneBDNDuqgBVrOsIjH2ycGJettPS4WnXkI5bcZVBFq1oy', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(93, 'Kasandra Al-Dawoud', 'Kaldwood@chamwings.com', 'KAD', NULL, '$2y$10$TNrVhofQqe84WWA8joOicewe/Cvm2lsb6maoGcLvkOOUxby2txtOK', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(94, 'Ranim Areija', 'rarija@chamwings.com', 'RAA', NULL, '$2y$10$9M.Og/yisgnDhV49YE9TpehfapooJiNJcdt.Bqmy7CpZK7yDNI2ki', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(95, 'Nuha Asad', 'nasad@chamwings.com', 'NAD', NULL, '$2y$10$zyJ9.BgaGIcN8wJTJ6B4r.5LnTi7v8SLFM.njACawFaV2OZ7n4YgO', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(96, 'Aya Keilani', 'Akelani@chamwings.com', 'AKI', NULL, '$2y$10$m8Zgem9crYKK3MjuEIBSJezk9flLm1JJ.HqMxFMaQ3/yy5URYd0ta', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(97, 'Leen AL-Dayeh', 'Laldayeh@chamwings.com', 'LDH', NULL, '$2y$10$j.XUouckVLGJaL0fSlieuOcAS3nloeyjPaIIN9AGwTYxOQu9jkJZS', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(98, 'Emad Durzi', 'edurzi@chamwings.com', 'EDI', NULL, '$2y$10$wvrd4EFG9MUC6R4ZrMOKyut9yx78hc8t18p/eqyMC6uTbHLtrCKY.', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(99, 'Nour Alaya', 'Nalaya@chamwings.com', 'NAA', NULL, '$2y$10$cN/dpkyiMAOrDZbX1IiJvuMMtXhOHx.878BE9XBlpbe.yrcrvyoQG', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:26'),
(100, 'Rana Naaem', 'rnaim@chamwings.com', 'RNM', NULL, '$2y$10$sniC62S.XDOrcIXf3ueBKeTVm.AyunIlIAWBPqRfHYSUHHUNEKTNG', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:27'),
(101, 'Mahmoud wattar', 'Malwattar@chamwings.com', 'MWR', NULL, '$2y$10$BsizxO5KgUF0JmIoQ9cO6O5P6CtkTeCwRMh0bpQ1XIwXawpgE/vzC', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:27'),
(102, 'Rahaf Harfoush', 'Rharfoush@chamwings.com', 'RHH', NULL, '$2y$10$Ouy40v9neB64vnmt4KVH9uTHRL2Z0SDZwCTtuKJAXhnlfHsAUyUXC', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:27'),
(103, 'Nour Aloundra Al-Doubol', 'neldihbal@chamwings.com', 'NDL', NULL, '$2y$10$QR22f0znvONMOHk7Xhfx8.dg701DYu/txwHKSdNilBeqyjTLoZyri', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:30'),
(104, 'Tarek Baza', 'Tbaza@chamwings.com', 'TBA', NULL, '$2y$10$PL6m1CC7uxadETsYH0s/.eGrGnRgyrz1Y.sMnXyKDlAAL0.AlWNHG', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:30'),
(105, 'Jouana Nasser', 'Jnasser@chamwings.com', 'JNR', NULL, '$2y$10$7SIhR7ooZdH.e68atcuBgO7CgkjNm/T0ctuJCNXB9xg53Pogzdl6C', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:30'),
(106, 'Mazen Naeem', 'mnaim@chamwings.com', 'MNM', NULL, '$2y$10$wIeUO4EAA670uOSLEEuZoODOf8Gx.pPReO5dt9OUNDFsal13iSVUK', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:30'),
(107, 'Sarah Sinou', 'Sasinou@chamwings.com', 'SSO', NULL, '$2y$10$6sMnCddYz8kXP4DMtEeQ6eTZ.dJS6XyIe5AaB9rBKam8u7oL.izjS', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:30'),
(108, 'Ranim Al-Hamwi', 'Rhamwi@chamwings.com', 'RHI', NULL, '$2y$10$za2u/hQJXaUF1L.j3aDRTO6.4lVS4A4RzRNyjcsiITbOHpYvC8QSe', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:30'),
(109, 'Mahmoud Sheiha', 'MSHIHA@chamwings.com', 'MSA', NULL, '$2y$10$3gI1.JYSzsSiAuFUnwaFauNb/8x/P2vZIcKwr7fOSVc.uhaAx4pJe', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:30'),
(110, 'Yamen Al-Moraif', 'Yalmoraif@chamwings.com', 'YAF', NULL, '$2y$10$w0AEJzmfGX5iItbVewuLpOk5Nmb3Xu4YyAe6xclpHTBU16pwDVy36', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(111, 'Manuella Khokazian', 'Mkhokazian@chamwings.com', 'MKN', NULL, '$2y$10$u.A/1JcqpUqWDmOlt6oftuSs4jgDs6dxSATLiFnw90vPzT7Wx5YDq', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(112, 'Dima Thamina', 'dthamina@chamwings.com', 'DTH', NULL, '$2y$10$4ocoV/Fduy3ltPREcq/0VOzzPdjNTw3SZU4i/nS73Ph3LxdRVx7YO', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(113, 'Diana Saoud', 'Dsaoud@chamwings.com', 'DSD', NULL, '$2y$10$jqK5d.XezOY10OQDd8L7G..1vJvna9qgmp1T9GM0QMYllhe0BM.JK', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(114, 'Ranim Eid', 'reid@chamwings.com', 'RED', NULL, '$2y$10$kRwv0Sg6KJkZxL0Wqfrotum85rV0cpz/NIUw95PyEDPvYZu9OipqO', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(115, 'Raghad Al-kaed', 'ralkaed@chamwings.com', 'RKD', NULL, '$2y$10$p/vyFNtiurG67UJzmNtUieY7uEHSo/Z.gn./t7UdYrIl211fktVru', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(116, 'Jouana Mefrej', 'Jmfarrej@chamwings.com', 'JMJ', NULL, '$2y$10$MyKySN3bJwyAYKk6RR3Yp.trVP2rDi0C4FfsLADkREIVNI3E2INSK', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(117, 'Merry Gabreil Al-Maalouf', 'Malmaalouf@chamwings.com', 'MGF', NULL, '$2y$10$sUaIpMGvIM.j3AZl8sJZsOXn7ohopqsfoCN7CWVBhtefk2NEDJAMG', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(118, 'Tarek Dahdal', 'Tdahdal@chamwings.com', 'TDL', NULL, '$2y$10$v5fa040jrb4Bv3DK288HxOXVq0WxG.KOp9/Mzamy0Twgd3Tc1NW5y', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(119, 'Ola Sulaiman', 'Osuleman@chamwings.com', 'OSN', NULL, '$2y$10$n5cSasGiXssTuRU5jRxI.euy/UZZnvzWqClg3jrn0eWhHCf6y.hLS', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(120, 'Youssef Dahman', 'Ydahman@chamwings.com', 'YDN', NULL, '$2y$10$VKdS27L2xcbRLtWuIV25Ue1Tc/f0ZhNEs8iKWOorFs45LpiU92vzu', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(121, 'Yara Al-Mazloum', 'yalmazloum@chamwings.com', 'YAM', NULL, '$2y$10$Jhr5uGAjmJXnieVKvgtfVO9LsAImWT8A24xBhPfcyJduNUbwGKDP6', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:31'),
(122, 'Mayssam Sheikh Al-Hara', 'Mshekhalhara@chamwings.com', 'MYA', NULL, '$2y$10$Kttovz0oGdMNn4gf6OLZ/elmGJLWfmPvS3EPJNzBAah4PFsp5ffYC', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:32'),
(123, 'Adel Ahmad', 'A.ahmad@chamwings.com', 'ADD', NULL, '$2y$10$blNoosTm0KUlEwdsY8IxiePDoZ7nPdLuN4.Pqlz82/tTaEYO0TyV.', 21, 2, 1, 1, NULL, NULL, NULL, '2020-06-25 07:52:28'),
(124, 'Batoul Shahin', 'Bshaheen@chamwings.com', 'BSN', NULL, '$2y$10$mu4KouuZOesWo8MlhNeVZuy30SahYEQgUl9TAe2aT8Jzcj5rt.uM.', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(125, 'Dalia Oud', 'Doud@chamwings.com', 'DOD', NULL, '$2y$10$3sSSiUdpkoq7BMYg.XidTOEw02aKLtvc.lK4UvNeVJc5Lr7y.Igx.', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(126, 'Duha Al-Kurdi', 'Dkurdi@chamwings.com', 'DKI', NULL, '$2y$10$xWqrW7xlPThsmQLIjRpQI.cCtKr.cGwRZXjsFSQ02Wx5Y5Cbsurya', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(127, 'Fadi Issa', 'fissa@chamwings.com', 'FIA', NULL, '$2y$10$NK67mIls4Xgav2QARmlAeu/daMAl7AcygW4dhNNy9E6e4uKH/bkSW', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(128, 'Maram hasan', 'mhasan@chamwings.com', 'MRN', NULL, '$2y$10$agnoyd5icBYEu8hVobsujeCHXvdjyVVkrjDBOt093vQknZlO56Bdy', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(129, 'Maya Al-Kadi', 'Mayalkadi@chamwings.com', 'MAI', NULL, '$2y$10$V6QVl/OlTnUala3pvc9ocuj9.DJ9XLmfJ9Fkr/X.wnG7xHwxiBiFS', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(130, 'Roshin Abdullah', 'Rabdullah@chamwings.com', 'ROA', NULL, '$2y$10$Rw3Uh2/wMCsxy8IJHVJ2XONPZFBMSY4rbh6rhxIX7whqE.azVjlFi', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(131, 'Farouk Hmeidan', 'Fhamidan@chamwings.com', 'FHN', NULL, '$2y$10$3tRtpdYg6vclwCX7Lv5v2e3/TjDyhSh9PAPxiiQId58O7m4t2Hbaq', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(132, 'Jouana Tahhan', 'Jtahhan@chamwings.com', 'JTN', NULL, '$2y$10$nb7ZBlKWp9pC9v4j2txc2eoXQgSASKXDj6WkewrAnAvKrFBn/78MO', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:35'),
(133, 'Marcel Ghiaa', 'Mghiea@chamwings.com', 'MGA', NULL, '$2y$10$f0/gieWYEeAxw9eGsDlJ4.7k8y3Y2xoQoCcB3Re9g776x7T5zKbFm', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36'),
(134, 'Rana Al- Ghalil', 'Ralghalel@chamwings.com', 'RGL', NULL, '$2y$10$WYm32FWtFlKmU0Lh73e.H.w6Fsgj2GQK4a5mOBMMjtRk8HcBEEn9u', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36'),
(135, 'Mouhamad Wasim Thiab', 'Wthiab@chamwings.com', 'WTB', NULL, '$2y$10$ncZrlwaW8w4SZEMZ8.WyoeB1sTUz160RUbRXfX8cZXhi1Vh7EURBe', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36'),
(136, 'Yara Khouzam', 'ykhouzam@chamwings.com', 'YKM', NULL, '$2y$10$spu9m4K3mRFQgvBG0PKhH.Cgruonni.Ic8sgoGqaEc0A6qGwOPqv2', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36'),
(137, 'Zakaria Al-Khateeb', 'Zalkhatib@chamwings.com', 'ZKB', NULL, '$2y$10$Sh.M9FwSVPpi0wLSWJz70O8DCrzpzUJ0WEQ4SbYfRWtOodfwx2xEe', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36'),
(138, 'Alaa Sulaiman', 'Alsulieman@chamwings.com', 'ASN', NULL, '$2y$10$ozuK0oEzer46n7G5BY5gDui7O/Ub6Io1o/kDKclNRweyr9zRRDnua', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36'),
(139, 'Hazar Al-Skeif', 'Hskief@chamwings.com', 'HAF', NULL, '$2y$10$sAhcSD1IsxkIxnHlTlBcMeNVNj.1VD9.QRmeBdoil7hIa31DCisou', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36'),
(140, 'Wadei Al-Saad', 'walsaad@chamwings.com', 'WAD', NULL, '$2y$10$vhKJA7q1R6jGwNtAXFM6JOSODSb9uKuahDu21R9aYo8TmeW4gl6Hq', 21, 2, 1, 0, NULL, NULL, NULL, '2020-03-29 05:17:36');

-- --------------------------------------------------------

--
-- Table structure for table `user_files`
--

DROP TABLE IF EXISTS `user_files`;
CREATE TABLE IF NOT EXISTS `user_files` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `is_read` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_files`
--

INSERT INTO `user_files` (`id`, `user_id`, `file_id`, `is_read`, `created_at`, `updated_at`) VALUES
(7, 39, 99, 1, '2020-07-02 09:21:19', '2020-07-02 09:21:19'),
(8, 39, 52, 1, '2020-07-02 09:22:13', '2020-07-02 09:22:13'),
(9, 39, 97, 1, '2020-07-02 09:22:38', '2020-07-02 09:22:38'),
(10, 39, 96, 1, '2020-07-02 09:39:37', '2020-07-02 09:39:37'),
(11, 1, 99, 1, '2020-07-05 13:49:16', '2020-07-05 13:49:16'),
(12, 1, 97, 1, '2020-07-05 13:59:25', '2020-07-05 13:59:25'),
(13, 1, 96, 1, '2020-07-05 14:12:44', '2020-07-05 14:12:44'),
(14, 1, 92, 1, '2020-07-05 14:13:45', '2020-07-05 14:13:45'),
(15, 1, 93, 1, '2020-07-05 14:14:39', '2020-07-05 14:14:39'),
(16, 1, 90, 1, '2020-07-05 14:37:09', '2020-07-05 14:37:09'),
(17, 1, 69, 1, '2020-07-05 14:39:31', '2020-07-05 14:39:31'),
(18, 1, 55, 1, '2020-07-05 14:44:39', '2020-07-05 14:44:39'),
(19, 39, 93, 1, '2020-07-05 14:45:18', '2020-07-05 14:45:18'),
(20, 39, 92, 1, '2020-07-05 14:45:39', '2020-07-05 14:45:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 39, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(2, 39, 16, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(3, 123, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(4, 73, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(5, 73, 16, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(6, 59, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(7, 59, 16, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(8, 1, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(9, 1, 16, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(10, 1, 19, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(11, 1, 22, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(12, 1, 21, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(13, 57, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(14, 57, 16, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(15, 57, 19, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(16, 49, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(17, 49, 16, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(18, 49, 19, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(19, 49, 22, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(20, 50, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(21, 50, 16, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(22, 96, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(23, 96, 19, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(24, 31, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(25, 27, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(26, 8, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(27, 38, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(28, 138, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(29, 47, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(30, 88, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(31, 42, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(32, 30, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(33, 74, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(34, 124, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(35, 37, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(36, 70, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(37, 12, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(38, 90, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(39, 21, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(40, 58, 18, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(41, 14, 19, '2020-07-02 03:06:10', '2020-07-02 03:06:10'),
(42, 11, 19, '2020-07-02 03:06:10', '2020-07-02 03:06:10');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `group_file`
--
ALTER TABLE `group_file`
  ADD CONSTRAINT `group_file_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_file_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `user_group_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_group_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
